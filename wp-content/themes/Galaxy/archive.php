<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Galaxy
 * @since 1.0
 * @version 1.0
 */

get_header(); 
?>
<?php echo view('tintuc.danhsach'); ?>

<?php get_footer();
