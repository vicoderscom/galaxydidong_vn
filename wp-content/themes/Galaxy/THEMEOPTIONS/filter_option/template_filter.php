<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/filter_option/style_filter.css">
<div class="theme_option_site">
    <h3 class="title_site">Filter option</h3>
    <form method="post" action="">
        <table>
            <h4>Tuỳ chỉnh price: </h4>
            <tr>
                <td>Price 1 </td>
                <td class="input_td">
                    <input type="text" name="price_1" value="<?php echo $price_value_1; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 2 </td>
                <td class="input_td">
                    <input type="text" name="price_min_2" value="<?php echo $price_value_min_2; ?>" />
                    <input type="text" name="price_max_2" value="<?php echo $price_value_max_2; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 3 </td>
                <td class="input_td">
                    <input type="text" name="price_min_3" value="<?php echo $price_value_min_3; ?>" />
                    <input type="text" name="price_max_3" value="<?php echo $price_value_max_3; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 4 </td>
                <td class="input_td">
                    <input type="text" name="price_4" value="<?php echo $price_value_4; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 5 </td>
                <td class="input_td">
                    <input type="text" name="price_min_5" value="<?php echo $price_value_min_5; ?>" />
                    <input type="text" name="price_max_5" value="<?php echo $price_value_max_5; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 6 </td>
                <td class="input_td">
                    <input type="text" name="price_min_6" value="<?php echo $price_value_min_6; ?>" />
                    <input type="text" name="price_max_6" value="<?php echo $price_value_max_6; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 7 </td>
                <td class="input_td">
                    <input type="text" name="price_min_7" value="<?php echo $price_value_min_7; ?>" />
                    <input type="text" name="price_max_7" value="<?php echo $price_value_max_7; ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 8 </td>
                <td class="input_td">
                    <input type="text" name="price_min_8" value="<?php echo $price_value_min_8; ?>" />
                    <input type="text" name="price_max_8" value="<?php echo $price_value_max_8 ?>" />
                </td>
            </tr>
            <tr>
                <td>Price 9 </td>
                <td class="input_td">
                    <input type="text" name="price_9" value="<?php echo $price_value_9 ?>" />
                </td>
            </tr>
            <tr>
                <td>Price max </td>
                <td class="input_td">
                    <input type="text" name="price_10" value="<?php echo $price_value_10 ?>" />
                </td>
            </tr>
            <tr>
                <td>Số điện thoại </td>
                <td class="input_td">
                    <input type="text" name="telephone" value="<?php echo $telephone ?>" />
                </td>
            </tr>
        </table>
       
        <input type="submit" name="save_filter_option" value="Save Theme Option" class="input_theme_option btn btn-primary" />
    </form>
</div>
