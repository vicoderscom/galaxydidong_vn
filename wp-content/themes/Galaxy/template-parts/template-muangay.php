<?php
/**
 * Template Name: Mua Ngay
 */


get_header();
global $post, $product, $woocommerce;

if(empty($_SESSION['wdm_user_custom_data'])) {
	echo view('checkout.faile');
} else {
	$cart_item_data['wdm_user_custom_data_value'] = $_SESSION['wdm_user_custom_data'];
	unset($_SESSION['wdm_user_custom_data']);

	if (empty($cart_item_data['wdm_user_custom_data_value']['product_id'])) {
	    wp_redirect(site_url('/'));
	}

	if (empty($cart_item_data['wdm_user_custom_data_value']['variable_id'])) {
	    wp_redirect(site_url('/'));
	}

	if (empty($cart_item_data['wdm_user_custom_data_value']['quantity']) || ($cart_item_data['wdm_user_custom_data_value']['quantity'] <= 0 || ($cart_item_data['wdm_user_custom_data_value']['quantity'] > 100))) {
	    $cart_item_data['wdm_user_custom_data_value']['quantity'] = 1;
	}

	$product_id = $cart_item_data['wdm_user_custom_data_value']['product_id'];
	$variable_id = $cart_item_data['wdm_user_custom_data_value']['variable_id'];
	$quantity = $cart_item_data['wdm_user_custom_data_value']['quantity'];

	$product = new WC_Product($product_id);
	$variable_product = new WC_Product($variable_id);

	$cart_id        = $woocommerce->cart->generate_cart_id( $product_id, $variable_id, $variable_product, $cart_item_data );
	$cart_item_key  = $woocommerce->cart->find_product_in_cart( $cart_id );

	$get_cart = $woocommerce->cart->get_cart();

	if ( !empty($get_cart[$cart_id]) ) {
	    $new_quantity = 1 + $woocommerce->cart->cart_contents[ $cart_item_key ]['quantity'];
	    $woocommerce->cart->set_quantity( $cart_item_key, $new_quantity, false );
	} else {
	    $cart_item_key = $cart_id;
	    $woocommerce->cart->cart_contents[ $cart_item_key ] = apply_filters( 'woocommerce_add_cart_item', array_merge( $cart_item_data, array(
	      'quantity'     => $quantity,
	    ) ), $cart_item_key );
	}
	$get_cart = $woocommerce->cart->get_cart();

	echo view('muangay.muangay', compact('woocommerce'));
}
get_footer();