<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
global $post;
?>
<h1 class="post-heading"><?php echo get_the_title(); ?></h1>
<p class="post-date"><?php  echo get_post_modified_time('l, F j, Y'); ?></p>
<div class="facebook-container">
	<span>
		<div class="fb-like" data-href="https://www.facebook.com/Galaxydidong-1084208671677995/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
	</span>
	<span>Like <a href="<?php echo site_url(); ?>"><b>Galaxydidong.vn</b></a> để được cập nhật tin siêu phẩm sắp ra mắt và khuyến mãi hấp dẫn</span>
</div>
<div class="post-content-main">
	<?php  echo the_content(); ?>
</div>
<div class="pull-right">
	Đăng bởi 
	<?php 
		echo "<span style='color: #2196F3;'>";
		$author_id=$post->post_author; 
		echo the_author_meta( 'user_nicename' , $author_id );
		echo "</span>"; 
	?>
</div>
