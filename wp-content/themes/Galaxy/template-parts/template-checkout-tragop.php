<?php
/**
 * Template Name: Checkout Trả góp
 */
get_header();
global $wpdb, $woocommerce, $wp_session;
if(!isset($_SESSION['order_id'])) {
	echo view('checkout.faile');
} else {
	$order_id = $_SESSION['order_id'];
	unset($_SESSION['order_id']);
	$datetime_now = current_time('Y-m-d');
	$order = new WC_Order( $order_id );
	$order->update_status('processing');
	$order_meta = get_post_meta($order_id);
	$items = $order->get_items();

	if(!empty($items)) {
		foreach ($items as $key => $item) {
			$get_cart[0]['quantity'] = $item['qty'];
			$get_cart[0]['wdm_user_custom_data_value']['variable_id'] = $item['variation_id']; 
			$get_cart[0]['wdm_user_custom_data_value']['data'] = [
				'root_price' => $item['line_subtotal_tax'],
				'calc_price' => $item['line_total'],
				'title' => $item['Tên sản phẩm'],
				'color' => $item['Màu sắc'],
				'title_chedobaohanh' => (isset($item['Chế độ bảo hành']) ? $item['Chế độ bảo hành'] : '-- Không chọn --'),
				'price_chedobaohanh' => (isset($item['Giá bảo hành']) ? $item['Giá bảo hành'] : '-- Không chọn --'),
				'loaimua' => $item['Loại mua'],
				'tratruoc' => (!empty($item['% trả trước']) ? $item['% trả trước'] : ''),
				'hantratruoc' => (!empty($item['Hạn trả góp']) ? $item['Hạn trả góp'] : ''),
				'tientratruoc' => (!empty($item['Tiền trả trước']) ? $item['Tiền trả trước'] : ''),
				'trahangthang' => (!empty($item['Trả hàng tháng']) ? $item['Trả hàng tháng'] : '')
			];
		}

		$post_data = [
			'billing_fullname' => $order_meta['_billing_fullname'][0],
			'billing_phone' => $order_meta['_billing_phone'][0],
			'billing_email' => $order_meta['_billing_email'][0],
			'billing_address' => $order_meta['_billing_address_1'][0]
		];
		echo view('../checkout/success', compact('woocommerce','order_id', 'post_data', 'get_cart', 'datetime_now'));
	} else {
		echo view('checkout.faile');
	}
	// echo "<pre>";
	// var_dump( $get_cart[0]['wdm_user_custom_data_value']['data']['title_chedobaohanh']);die;

	// if(isset($_POST)) {
	//     $datetime_now = current_time('Y-m-d');
	//     // function get_id_order_tragop() {
	//     	// global $wpdb, $woocommerce;
	// 		try {
	// 		    wc_transaction_query( 'start' );
	// 		    if(!isset($_POST['product_id']) || empty($_POST['product_id']) ) {
	// 		        return view('../checkout/faile');
	// 		    }

	// 		    $_product = new WC_Product_Variation( $_POST['variable_id'] );
	// 		    // if(empty($_product)) {
	// 		    //     echo "$_POST"; exit;
	// 		    // }
	// 		    $str_query_chedo = "SELECT ID, post_title FROM ".($wpdb->prefix)."posts WHERE post_status='publish' and post_type='chedobaohanh' and ID=".$_POST['chedobaohanh'];
	// 		    echo $str_query_chedo; die;
	// 		    $chedobaohanh = $wpdb->get_row($str_query_chedo);

	// 		    echo "<pre>";
	// 		    var_dump($chedobaohanh);die;

	// 		    $chedobaohanh_price = '';
	// 		    $chedobaohanh_name = '';
	// 		    if(!empty($str_query_chedo)) {
	// 		        $chedobaohanh_name = $chedobaohanh->post_title;
	// 		        $chedobaohanh_price = get_field('gia_tien', $chedobaohanh->ID);
	// 		    }
	// 		    $name_product = $_product->post->post_title;
	// 		    $root_price = $_product->get_price();
	// 		    $name_color = $_POST['name_color'];
	// 		    $sum_price = $root_price + $chedobaohanh_price;
	// 		    $expire_tragop = $_POST['choose-pack-tragop'];
	// 		    $precent_tratruoc = $_POST['choose-tratruoc'];

	// 		    $order_data = array(
	// 		        'status'        => apply_filters( 'woocommerce_default_order_status', 'pending' ),
	// 		        'customer_note' => isset( $_POST['order_comments'] ) ? $_POST['order_comments'] : '',
	// 		        'cart_hash'     => '',
	// 		        'created_via'   => 'tragop',
	// 		    );

	// 		    $order = wc_create_order( $order_data );
	// 		    if ( is_wp_error( $order ) ) {
	// 		        throw new Exception( sprintf( __( 'Lỗi %d: Không tạo được hóa đơn. Xin vui lòng thử lại', 'woocommerce' ), 520 ) );
	// 		    } elseif ( false === $order ) {
	// 		        throw new Exception( sprintf( __( 'Lỗi %d: Không tạo được hóa đơn. Xin vui lòng thử lại', 'woocommerce' ), 521 ) );
	// 		    } else {
	// 		        $order_id = $order->id;
	// 		        do_action( 'woocommerce_new_order', $order_id );
	// 		    }

	// 		    $item_id = $order->add_product(
	// 		        $_product,
	// 		        1,
	// 		        array(
	// 		            'variation' => '',
	// 		            'totals'    => array(
	// 		                'subtotal'     => $sum_price,
	// 		                'subtotal_tax' => $root_price,
	// 		                'total'        => $sum_price,
	// 		                'tax'          => 0,
	// 		                'tax_data'     => 0
	// 		            )
	// 		        )
	// 		    );

	// 		    if ( ! $item_id ) {
	// 		        throw new Exception( sprintf( __( 'Lỗi %d: Không tạo được hóa đơn. Xin vui lòng thử lại.', 'woocommerce' ), 525 ) );
	// 		    }

	// 		    wc_add_order_item_meta($item_id, 'Loại mua', 'Trả góp');
	// 		    if (!empty($name_product)) {
	// 		        wc_add_order_item_meta($item_id, 'Tên sản phẩm', $name_product);
	// 		    }
	// 		    if (!empty($root_price)) {
	// 		        wc_add_order_item_meta($item_id, 'Giá gốc', wc_price($root_price));
	// 		    }
	// 		     wc_add_order_item_meta($item_id, 'Số lượng', '1');
	// 		    if (!empty($name_color)) {
	// 		        wc_add_order_item_meta($item_id, 'Màu sắc', $name_color);
	// 		    }
	// 		    if (!empty($chedobaohanh_name)) {
	// 		        wc_add_order_item_meta($item_id, 'Chế độ bảo hành', $chedobaohanh_name);
	// 		    }
	// 		    if (!empty($chedobaohanh_price)) {
	// 		        wc_add_order_item_meta($item_id, 'Giá bảo hành', wc_price($chedobaohanh_price));
	// 		    }
	// 		    if(!empty($precent_tratruoc)){
	// 		        wc_add_order_item_meta($item_id, '% trả trước', $precent_tratruoc . '%');
	// 		    }
	// 		    if(!empty($expire_tragop)){
	// 		        wc_add_order_item_meta($item_id, 'Hạn trả góp', $expire_tragop . ' tháng');
	// 		    }
	// 		    wc_add_order_item_meta($item_id, 'Tiền trả trước', wc_price(($root_price * $precent_tratruoc)/100));
	// 		    $tien_tra_hangthang =  ($root_price - (($root_price * $precent_tratruoc)/100))/$expire_tragop;
	// 		    wc_add_order_item_meta($item_id, 'Trả hàng tháng', wc_price(abs($tien_tra_hangthang)).'/tháng');
	// 		    wc_add_order_item_meta($item_id, 'Tổng trả', wc_price($sum_price));

	// 		    // Billing address
	// 		    $billing_address = array();
	// 		    if(!empty($_POST['billing_fullname'])) {
	// 		        $billing_address[ 'fullname' ] = $_POST['billing_fullname'];
	// 		    } else {
	// 		        $billing_address[ 'fullname' ] = '';
	// 		    }
	// 		    if(!empty($_POST['billing_email'])) {
	// 		        $billing_address[ 'email' ] = $_POST['billing_email'];
	// 		    } else {
	// 		        $billing_address[ 'email' ] = '';
	// 		    }
	// 		    if(!empty($_POST['billing_phone'])) {
	// 		        $billing_address[ 'phone' ] = $_POST['billing_phone'];
	// 		    } else {
	// 		        $billing_address[ 'phone' ] = '';
	// 		    }
	// 		    if(!empty($_POST['billing_address'])) {
	// 		        $address = $_POST['billing_address'];
	// 		    } else {
	// 		        $address = '';
	// 		    }
	// 		    if(!empty($_POST['billing_provice'])) {
	// 		        $provice = $_POST['billing_provice'];
	// 		    } else {
	// 		        $provice = '';
	// 		    }
	// 		    if(!empty($_POST['billing_district'])) {
	// 		        $district = $_POST['billing_district'];
	// 		    } else {
	// 		        $district = '';
	// 		    }
	// 		    $billing_address[ 'address_1' ] = $address.', '.$district.', '.$provice;

	// 		    $order->set_address( $billing_address, 'billing' );

	// 		    wc_transaction_query( 'commit' );

	// 		} catch ( Exception $e ) {
	// 		    wc_transaction_query( 'rollback' );
	// 		    return new WP_Error( 'checkout-error', $e->getMessage() );
	// 		}
	// 		// return $order_id;
	//     // }

	// 	// $order_id = get_id_order_tragop();
	// 	if(!empty($order_id)) {
	// 		$post_data = $billing_address;
	// 		$get_cart[0]['quantity'] = 1;
	// 		$get_cart[0]['wdm_user_custom_data_value']['variable_id'] = $_POST['variable_id']; 
	// 		$get_cart[0]['wdm_user_custom_data_value']['data'] = [
	// 			'root_price' => $root_price,
	// 			'calc_price' => $root_price,
	// 			'title' => $name_product,
	// 			'color' => $name_color,
	// 			'title_chedobaohanh' => $chedobaohanh_name,
	// 			'price_chedobaohanh' => $chedobaohanh_price
	// 		];
	// 	    echo view('../checkout/success', compact('woocommerce','order_id', 'post_data', 'get_cart', 'datetime_now'));
	// 	} else {
	// 	    return new WP_Error( 'checkout-error', $e->getMessage() );
	// 	}
	// }
	// else {
	// 	echo view('checkout.faile');
	// }
}
get_footer();