<?php
/**
 * Template Name: Trả góp
 */
get_header();
global $woocommerce, $product;

if(!isset($_POST['product-id']) || empty($_POST['product-id']) ) {
	throw new WP_Error("Không tìm thấy trang !", 404);
}

$product = new WC_Product($_POST['product-id']);

if(!empty($product)) {
	$init_variation = new WC_Product_Variable($_POST['product-id']);
	$available_variations = $init_variation->get_available_variations();
	if( !empty($available_variations) ){
		echo view('tragop.tragop', compact('product', 'available_variations'));
	}
} else {
	throw new WP_Error("Không tìm thấy trang !", 404);
}

get_footer();