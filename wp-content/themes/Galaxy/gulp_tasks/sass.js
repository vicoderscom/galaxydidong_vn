var gulp = require('gulp');
var sass = require('gulp-sass');
var globImporter = require('sass-glob-importer');
module.exports = function () {
  return gulp.src('./assets/css/scss/app.scss')
    .pipe(sass({importer: globImporter()}).on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
};
