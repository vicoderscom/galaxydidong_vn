var gulp = require('gulp');
var watch = require('gulp-watch');
var livereload = require('gulp-livereload')
module.exports = function () {
  livereload.listen();
  watch(['assets/js/**/*.js'], function () {
    gulp.run([
      'jshint',
      'scripts',
      'browserify'
    ]);
  });
  gulp.watch('./assets/css/**/*.scss', ['sass']);
  gulp.watch('./assets/css/**/*.scss', ['minify_css']);

  gulp.watch(['./dist/css/*.css'], function (files) {
    livereload.changed(files)
  });


}
