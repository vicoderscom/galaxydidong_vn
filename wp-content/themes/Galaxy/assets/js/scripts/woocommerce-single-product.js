$(document).ready(function(){
	var data_price_calc = $('#select-color li.active').attr('data-price-calc');
	$('.get_price').html(format_price(data_price_calc) + ' ' + currency_symbol);

	var result_calc_tragop = (data_price_calc * 0.3);
	var tmp_price_calc = result_calc_tragop;
	$('.result_calc_tragop').html(format_price(result_calc_tragop) + currency_symbol);
	$('#select-color li').click(function(){
		$('#select-color li').removeClass('active');
		$(this).addClass('active');
		var img_root_src = $('.image-main img').attr('src');
		var variable_id = $(this).attr('data-variable');
		var price_html = $(this).attr('data-price');
		var image_src = $(this).attr('data-img-src');
		var name_color = $(this).attr('data-color');

		$('#inp-name-color').val(name_color);
		data_price_calc = $(this).attr('data-price-calc');

		if(typeof data_price_calc === "undefined"){
			result_calc_tragop = tmp_price_calc;
		} else {
			result_calc_tragop = format_price(data_price_calc * 0.3);
		}
		$('.result_calc_tragop').html(result_calc_tragop + currency_symbol);

		var data_price_baohanh = 0;
		data_price_baohanh = $('#pa_chedobaohanh option:selected').attr('data-price-baohanh');
		// console.log(data_price_baohanh);
		data_price_calc = parseFloat(price_html) + parseFloat(data_price_baohanh);
		$('.price-main').html(format_price(data_price_calc) + currency_symbol);
		$('.price-main').attr('data-price-calc', parseFloat(data_price_calc));
		$('#inp-variable-id').val(variable_id);
		if(!image_src || image_src.length > 10) {
			$('.image-main img').attr('src', image_src);
		} else {
			$('.image-main img').attr('src', img_root_src);
		}

		var tratruoc_expire = $('.choose-pack-tragop').val();
		var tratruoc_percent = $('.choose-tratruoc').val();

		price_tragop(data_price_calc, tratruoc_percent, tratruoc_expire);
	});

	//============ for delete item on cart via ajax =============
	$('.btn-delete-item-cart').click(function(){
		var data_item_key = $(this).attr('data-item-key');
		var data_parent = $(this).attr('data-parent');
		var str = '';
		// console.log(data_item_key);
		// console.log(data_parent);
		var conf = confirm('Bạn có chắc muốn xóa sản phẩm này trong giỏ hàng !');
		if(conf === true){
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: {
					action: 'delete_item_cart',
			        data_item_key:   data_item_key
				}, 
				success: function (response) {
					var result = JSON.parse(response);
					$('.woocommerce-message').html('');
					if(response.code == "200"  || result === 200) {
						str += '<div class="woocommerce-message">';
						str += 'Đã xóa sản phẩm thành công';
						str += '</div>';
						$('.'+data_parent).html('');
						$('.custom-message').html(str);
					} else {
						str += '<div class="alert alert-danger">';
						str += 'Không tồn tại sản phẩm cần xóa trong giỏ hàng !';
						str += '</div>';
						$('.custom-message').html(str);
					}
				}
			});
		}
	});
	//============ for delete all item on cart via ajax =============
	$('.btn-delete-all-cart').click(function(){
		var data_delete = $(this).attr('data-delete');
		var str = '';
		var conf = confirm('Bạn có chắc muốn xóa tất cả giỏ hàng !');
		if(conf === true){
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: {
					action: 'delete_all_cart',
			        data_delete: data_delete
				}, 
				success: function (response) {
					var result = JSON.parse(response);
					$('.custom-message').html('');
					$('.woocommerce-message').html('');
					if(result === 200 || result === "200") {
						str += '<div class="woocommerce-message">';
						str += ' Giỏ hàng đã được xóa thành công';
						str += '</div>';
						console.log(str);
						$('.section-product-container').html('<div class="row"><p> Không có sản phẩm nào trong giỏ hàng ...</p></div>');
						$('.custom-message').html(str);
					} else {
						str += '<div class="alert alert-danger">';
						str += 'Giỏ hàng trống !';
						str += '</div>';
						$('.custom-message').html(str);
					}
				}
			});
		}
	});
	//============ for button "Mua ngay " =======================
	$('#pa_chedobaohanh').change(function(){
		var data_price_baohanh = $('option:selected', this).attr('data-price-baohanh');
		data_price_calc = $('#select-color li.active').attr('data-price-calc');

		var get_price = $('.get_price').attr('data-price');
		var price_main_tragop = $('.price-main').attr('data-price-calc');
		var sum_price = (parseFloat(data_price_calc) + parseFloat(data_price_baohanh));
		var sum_price_tragop = (parseFloat(data_price_calc) + parseFloat(data_price_baohanh));
		// var str = ' + ' + (format_price(data_price_baohanh)) + currency_symbol;
		$('.get_price').html(format_price(sum_price) + currency_symbol);
		$('.price-main').html(format_price(sum_price_tragop) + currency_symbol);

		//===== change price tragop =====
		var tratruoc_expire = $('.choose-pack-tragop').val();
		var tratruoc_percent = $('.choose-tratruoc').val();

		price_tragop(sum_price_tragop, tratruoc_percent, tratruoc_expire);
	});

	// =================== for tragop ==================
	var price_calc = 0;
    var first_select_tratruoc = $('.choose-tratruoc option:first').val();
    var first_select_expire = $('.choose-pack-tragop option:first').val();
    
    console.log(price_calc);
    $('.choose-tratruoc').change(function(){
		price_calc = $('.price-thanhtoan .price-main').attr('data-price-calc');
        var tratruoc = 0;
        var eachmonth = 0;
        var tratruoc_percent = $(this).val();
        var tratruoc_expire = $('.choose-pack-tragop').val();
        
        price_tragop(price_calc, tratruoc_percent, tratruoc_expire);
        // console.log(tratruoc, eachmonth);
    });

    $('.choose-pack-tragop').change(function(){
    	price_calc = $('.price-thanhtoan .price-main').attr('data-price-calc');
        var tratruoc_2 = 0;
        var eachmonth_2 = 0;
        var tratruoc_expire_2 = $(this).val();
        var tratruoc_percent_2 = $('.choose-tratruoc').val();

        console.log(price_calc);
        price_tragop(price_calc,tratruoc_percent_2, tratruoc_expire_2);
        // console.log(tratruoc_2, eachmonth_2);
    });
	function price_tragop(price_calc, tratruoc_percent, tratruoc_expire){
		var tratruoc = price_calc * (tratruoc_percent/100);
        var eachmonth = (price_calc - (tratruoc))/tratruoc_expire;

        console.log(price_calc);
		$('.price-tratruoc').html(format_price(tratruoc) + currency_symbol);
        $('.price_each-month').html(format_price(eachmonth) + currency_symbol);
	}
});