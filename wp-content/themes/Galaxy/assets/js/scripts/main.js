jQuery(function($) {

    $('.slidetop').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });


    var videowidth = $(".video-container").width();
    var videoheight = videowidth / 1.69;

    
    $(window).on('resize', function() {
        var videowidth = $(".video-container").width();
        var videoheight = videowidth / 1.69;

        $(".video-container").css("height", videoheight);
    });

    $(".video-container").css("height", videoheight);

    $(window).scroll(function() {
        var scr = window.innerWidth;
        var distance = $(document).height() - $(document).scrollTop();
        if (distance > 1400 && scr > 991) {
            $(".thanhlienhe").fadeIn();

        }
        //  else {
        //     $(".thanhlienhe").fadeOut();
        // }

        var videowidth = $(".video-container").width();
        var videoheight = videowidth / 1.65;

        $(".video-container").css("height", videoheight);
    });

    function menu_resize() {
        var screen_menu_cat = window.innerWidth;
        var element_menu = $('.menu_wrapper .list_option_product');
        var element_menu_child = element_menu.children('ul');
        var element_menu_child_all = element_menu.find('ul');
        var total_menu_width = element_menu.width();
        var menu_width = element_menu_child.width() / 2;
        var height_menu_child = element_menu_child.height();
        if(screen_menu_cat > 992) {
            element_menu_child.css({
                'margin-left': -menu_width,
            });
            element_menu_child_all.css({
                'opacity': 1,
            });
            element_menu.css({
                'height': height_menu_child
            });
        } else {
            element_menu_child_all.css({
                'opacity': 1,
            });
            element_menu_child.css({
                'left':'0',
                'width': '100%',
                'background-color': '#ecf0f1'
            });
        }
    }

    menu_resize();
    $(window).resize(function() {
        menu_resize();
    });

    $('#item_product_cat .item_product').hover(function() {
        $(this).find('.info_hover').show();
    }, function() {
        $(this).find('.info_hover').hide();
    });

    $('.list_option_product > ul > li').hover(function() {
        $(this).children('ul').show();
    }, function() {
        $(this).children('ul').hide();
    });

    $('.list_option_product > ul > li').click(function() {
        $(this).children('ul').toggle();
    });
    setTimeout(function() {
        var window_screen_default = window.innerWidth;

        function slider_resize() {
            var slider_height = $("#rev-slider-widget-2").height();
            var images_height = $(".div-anh").height();
            var video_height = slider_height - images_height;
            $('.video-container').css('height', video_height);
            $('.video-container iframe').css('height', video_height);
        }

        function slider_mobile() {
            var images_height = $(".div-anh").height();
            $('.video-container').css('height', images_height);
            $('.video-container iframe').css('height', images_height);
        }

        function set_resize() {
            var window_screen = window.innerWidth;
            if (window_screen > 992) {
                slider_resize();
            } else {
                slider_mobile();
            }
        }
        if (window_screen_default > 992) {
            slider_resize();
        } else {
            slider_mobile();
        }

        $(window).resize(function() {
            set_resize();
        });
        $(window).scroll(function() {
            set_resize();
        });
    }, 100);

    function set_img_cat() {
        var img_advertiment = $('.item_product_qq');
        var cat_item = $('.border-product').height();
        img_advertiment.css('height', cat_item + 17);
    }

    function get_img_cat() {
        var window_screen_cat = window.innerWidth;
        if (window_screen_cat > 992) {
            set_img_cat();
        } else {
            $('.item_product_qq').css('height', 'auto');
        }
    }
    $(window).resize(function() {
        get_img_cat();
    });
    get_img_cat();
    // function hover_menu_site() {
    //     var set_window_menu_site = window.innerWidth;
    //     if(set_window_menu_site > 1200) {
    //         $('#menu-menu-chinh li').hover(function(){
    //             $(this).children('ul').show();
    //         }, function(){
    //             $(this).children('ul').hide();
    //         });
    //     } else {
    //         $('#menu-menu-chinh li').click(function(){
    //             $('#menu-menu-chinh li ul').hide();
    //             $(this).children('ul').toggle();
    //         });
    //     }
    // }
    // hover_menu_site();
    
    var num_item = $('#item_product_cat .item_product').length;
    if(num_item === 0) {
        $('#no_product_show').html('Không tìm thấy sản phẩm.');
    }
    
    var num_item_img_view = $('.list_image_product_detail .sub-item').length;
    if(num_item_img_view === 0) {
        $('.arrow-container').hide();
    }

    // menu
    function res_menu_site() {
        var menu_list_site = $('#menu-menu-chinh').width();
        var menu_list_site_all = $('#navbar_menu_site').width();
        var menu_search_site = $('#navbar_menu_site .search_phone').width();
        var menu_logo_site = $('#navbar_menu_site .navbar-header').width();
        var menu_set_site = menu_list_site_all - menu_logo_site - menu_logo_site;
        var window_screen = $(window).innerWidth();
        if(menu_list_site > (menu_set_site-100) && window_screen > 768 && window_screen < 1200) {
            $('.menu_site').addClass('set_repons_menu');
            $('.tabled_button').show();
        } else {
            $('.menu_site').removeClass('set_repons_menu');
            $('.tabled_button').hide();
        }
    }
    res_menu_site();
    $(window).resize(function(){
        // hover_menu_site();
        res_menu_site();
    });
    $('.tabled_button').click(function(){
        $('.set_repons_menu').find('#menu-menu-chinh').toggle();
    });
});

$(document).ready(function(){
    $( "#aaa" ).click(function() {
      $(".thanhlienhe").slideToggle();
    });
});
// $(document).ready(function(){
//     var quang = screen.height;
//     console.log(quang);

// });