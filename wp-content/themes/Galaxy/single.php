<?php
/**
 * The template for displaying all single posts
 */
global $post;
get_header();
$query = new WP_Query( array(
    'meta_key' => 'post_views_count',
    'orderby' => 'meta_value_num',
    'posts_per_page' => 10
) );

?>
<section id="news">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">
					<a href="<?php echo site_url(); ?>">Home</a>&nbsp;/&nbsp;<a href="<?php echo site_url('tin-tuc') ?>">Tin tức</a>&nbsp;/&nbsp;<span class="post-title-insert"></span>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="main-content col-md-8 col-sm-8 col-xs-12">
				<div class="heading">
					<p>TIN TỨC</p>
				</div>
				<div class="post-content">
					<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/post/content', get_post_format() );

						endwhile;
					?>
				</div>
				<div class="clearfix"></div>
				<div class="comment">
					<div class="heading">
						<p>>> Đánh giá và bình luận</p>
					</div>
					<div class="comment-fb">
						<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-width="100%" data-numposts="5"></div>
					</div>
				</div>
			</div>
			<div class="sidebar col-md-4 col-sm-4 col-xs-12">
				<?php 
					if(is_active_sidebar('siderbar-news')) {
						dynamic_sidebar('siderbar-news');
					}
				?>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var title_post_insert = $('h1.post-heading').text();
		if(title_post_insert !== '' || title_post_insert !== null)  {
			$('span.post-title-insert').text(title_post_insert);
		}
	});
</script>
<?php
get_footer();
