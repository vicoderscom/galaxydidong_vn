@php
	global $post, $wp_query;
	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$args = [
	    'post_type' => 'post',
	    'posts_per_page' => 3,
	    'post_status' => ['publish'],
	    'paged' => $paged,
	];
	$tintuc = new WP_Query($args);
	$total_pages = $tintuc->max_num_pages;
@endphp
<section id="list-post-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php do_action('woocommerce_before_main_content');?></p>
			</div>
		</div>
		<div class="row">
			<div class="list-post col-md-8 col-sm-8 col-xs-12">
				<div class="heading">
					<p>TIN TỨC</p>
				</div>
				<div class="content">
					@foreach($tintuc->posts as $key => $tin)
					<div class="row row-post">
						<div class="col-md-4 col-sm-4 col-xs-12 col-left">
							<a href="@php the_permalink($tin->ID) @endphp"><img class="img-responsive" src="@php echo wp_get_attachment_url(get_post_thumbnail_id($tin->ID)); @endphp"></a>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-12 col-right">
							<a href="@php the_permalink($tin->ID) @endphp"><div class="title">{!! $tin->post_title !!}</div></a>
							<p> {!! str_limit($tin->post_excerpt, 150) !!} </p>
							<div class="row pull-right xem-tiep"><a href="@php the_permalink($tin->ID) @endphp"">Xem tiếp ...</a></div>
						</div>
					</div>
					<hr>
					@endforeach
				</div>
				<div class="row" style="margin-left: 0; margin-right: 0;">
					<div class="paginate pull-right">
						@php
							do_action('garung_paginate', $paged, $total_pages);
						@endphp	
					</div>
				</div>
			</div>
			<div class="sidebar col-md-4 col-sm-4 col-xs-12 ">
				@php
				if (is_active_sidebar('siderbar-news')) {
				    dynamic_sidebar('siderbar-news');
				}
				@endphp
			</div>
			<div class="sidebar col-md-4 col-sm-4 col-xs-12 ">
			    <div class="heading">
		            <p>Tin tức nổi bật</p>
		        </div>
		        @php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $news_heighlight = array(
                    	'post_type' => 'post',
                        'posts_per_page'=>5,
                        'paged'=>$paged
                    );
                    $loop_news = new WP_Query($news_heighlight); //var_dump($loop_blog);
                    if($loop_news->have_posts()) {
                        while($loop_news->have_posts()) : $loop_news->the_post();
                            $image_link = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); 
                            $meta_highlight = get_post_meta($post->ID);
                            if(isset($meta_highlight['tin_noi_bat'][0]) && !empty($meta_highlight['tin_noi_bat'][0])) {
                            @endphp
		                        <div class="post-container">
					                <div class="post">
					                    <div class="row tin-row" style="margin-bottom: 15px;">
					                        <div class="col-sm-4">
					                            <a href=""><img class="img-responsive" src="{{$image_link}}"></a>
					                        </div>
					                        <div class="col-sm-8">
					                            <p><a onMouseOver="this.style.color='#2196f3'" onMouseOut="this.style.color='#000'" href="">{{the_title()}}</a></p>
					                        </div>
					                    </div>
					                </div>
					            </div>
	                        @php
	                        }
	                    endwhile;
                        @endphp
                        @php
                    }
                @endphp
			</div>
		</div>
	</div>
</section>

