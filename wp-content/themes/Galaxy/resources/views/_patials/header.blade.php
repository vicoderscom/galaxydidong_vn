<div class="menuu">
   
    <div class="container">
        <div class="row ">
            <nav class="navbar navbar-default" role="navigation" id="navbar_menu_site">
                <div class="container">
                    <div class="searchform-wrapper">
                    </div>
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                            <img src="{!! $logo_upload !!}" alt="">
                        </a>

                    </div>
                    <div class="tabled_button">
                        <span class="glyphicon glyphicon-align-justify"></span>
                    </div>
                     <div class="search_phone">
                            <div class="serach-bar">
                                <div class="hotline text-uppercase">
                                    Hotline: <a href="tel:0962469899">{!! get_option('telephone') !!}</a>
                                </div>
                            </div>
                            <div class="searchh">
                                <?php get_search_form(); ?>
                            </div>
                        </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php /* Primary navigation */
                    wp_nav_menu(array(
                        'theme_location' => 'mainmenuu',
                        'depth' => 2, 
                        'container' => 'div',
                        'container_class' => 'collapse navbar-collapse navbar-ex1-collapse menu_site',
                        'menu_class' => 'nav navbar-nav', //class của thẻ ul
                        'walker' => new wp_bootstrap_navwalker()) //Cái này để nguyên, không thay đổi
                    );
                    ?>
                </div>
            </nav>

        </div>
    </div>
</div>

</div>

<div class="div-header hidden">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 menu-chinh">
                <nav class="navbar navbar-default" role="navigation">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="{!! $logo_upload !!}" alt="">
                        </a>
                    </div>

                </nav>

            </div>
            
            <div class="col-md-2 serach-bar">
                <div class="hotline hidden-xs hidden-sm text-uppercase">
                    Hotline: 0123456789
                </div>
                <div class="search-div">
                    <input type="text">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="my_phone">
    <div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
    <div class="phonering-alo-ph-circle"></div>
     <div class="phonering-alo-ph-circle-fill"></div>
    <a  class="pps-btn-img" title="Liên hệ">
        <div class="phonering-alo-ph-img-circle" id="aaa"></div>
     </a>
    </div>
</div>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="208411750008120">
</div>



