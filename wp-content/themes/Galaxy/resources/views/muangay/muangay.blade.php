@php

	$args = [
		'posts_per_page' => 3,
		'post_type' => 'phuongthucthanhtoan',
		'order' => 'ASC',
		'orderBy' => 'ID'
	];
	$phuongthucthanhtoans = new WP_Query($args);

	$get_cart = $woocommerce->cart->get_cart();

	$product_id = 0;
	$variable_id = 0;
	$name_color = "";

	if(!empty($get_cart)) {
		$data_delete_cart = 1;
	} else {
		$data_delete_cart = 0;
	}
@endphp
<div id="muangayPage">
	<div class="container">
		<div class="row breadcrumb" style="">
			@php  woocommerce_breadcrumb(); @endphp
		</div>
		<div class="row">
		{{-- @php do_action( 'woocommerce_before_single_product' ); @endphp --}}
			<div class="custom-message"></div>
		</div>
		<div class="product-tragop section-common">
			<div class="row title-product-tragop" style="border: 1px solid #eee; margin-bottom: 20px">
				<div class="title-review title-common" style=""><i class="fa fa-mobile" aria-hidden="true"></i> 1 . Thông tin sản phẩm
				</div>
			</div>
			@if(!empty($get_cart))
				<div class="section-product-container">
					@php 
						$total = 0;  
						$array_color = ['black', 'white', 'blue', 'green', 'grey', 'pink', 'red', 'yellow'];
						
					@endphp
					@foreach($get_cart as $key_cart => $cart_item_data)
					    @php
					        // $cart_item_num = $cart_item_data['quantity'];
					        // if(isset($cart_item_num))
					        // var_dump($cart_item_num);
					    @endphp
						@php
							$product_id = $cart_item_data['wdm_user_custom_data_value']['product_id'];
							$variable_id = $cart_item_data['wdm_user_custom_data_value']['variable_id'];
							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['title'])) {
								$title = $cart_item_data['wdm_user_custom_data_value']['data']['title'];
							} else {
								$title = '#NoName';
							}

							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['root_price'])) {
								$root_price = $cart_item_data['wdm_user_custom_data_value']['data']['root_price'];
							} else {
								$root_price = 0;
							}

							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['calc_price'])) {
								$calc_price = $cart_item_data['wdm_user_custom_data_value']['data']['calc_price'];
							} else {
								$calc_price = 0;
							}

							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['display_price'])) {
								$display_price = $cart_item_data['wdm_user_custom_data_value']['data']['display_price'];
							} else {
								$display_price = 0;
							}

							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['image_link'])) {
								$image_link = $cart_item_data['wdm_user_custom_data_value']['data']['image_link'];
							} else {
								$image_link = 'https://fakeimg.pl/130x190';
							}

							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['color'])) {
								$color = $cart_item_data['wdm_user_custom_data_value']['data']['color'];
							} else {
								$color = '';
							}
							$name_color = $color;
							if(in_array($name_color, $array_color)) {
								$check_color = true;
							} else {
								$check_color = false;
							}

							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['title_chedobaohanh'])) {
								$title_chedobaohanh = $cart_item_data['wdm_user_custom_data_value']['data']['title_chedobaohanh'];
							} else {
								$title_chedobaohanh = '';
							}
							if(!empty($cart_item_data['wdm_user_custom_data_value']['data']['price_chedobaohanh'])) {
								$price_chedobaohanh = $cart_item_data['wdm_user_custom_data_value']['data']['price_chedobaohanh'];
							} else {
								$price_chedobaohanh = 0;
							}
							$total += $calc_price * $cart_item_data['quantity'];

							$chedobaohanh = $cart_item_data['wdm_user_custom_data_value']['chedobaohanh'];

						@endphp
						<div class="row padding-section item-{!! $key_cart !!} item-common">
							<div class="col-md-2 col-sm-3 col-xs-12 image-item">
								<div class="image-product">
									<img src="{!! $image_link !!}">
								</div>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-12 detail-item">
								<div class="col-md-12 col-sm-12 col-xs-12 name-product-container" style="">
									<div class="name-product">
										<h4>{!! $title !!}</h4>
									</div>
								</div>
								@if(!empty($color))
								<div class="col-md-12 col-sm-12 col-xs-12 choose-color">
									<p style="float: left;">Màu sắc : </p>&nbsp<p
												style="
												background-color: {!! $color !!};
												width: 25px;
											    height: 25px;
											    border-radius: 50%;
											    float: left;
											    list-style: none;
											    margin-right: 10px;" class="@if(!$check_color){{ 'price_other active' }}@endif">
											@if(!$check_color)
												@php
													$meta = get_post_meta($variable_id, 'attribute_pa_color', true);
													$term = get_term_by('slug', $meta, 'pa_color');
													echo $term->name;
												@endphp
											@endif
										    </p>
								</div>
								@endif
								@if(!empty($cart_item_data['quantity']))
								<div class="col-md-12 col-sm-12 col-xs-12">
									Số lượng: <b>{!! $cart_item_data['quantity'] !!}</b>
								</div>
								@endif
								@if(!empty($title_chedobaohanh))
								<div class="col-md-12 col-sm-12 col-xs-12">
									Chế độ bảo hành: <b>{!! $title_chedobaohanh !!}</b>
								</div>
								@endif
								<div class="col-md-12 col-sm-12 col-xs-12">
									Đơn giá: <span >{!! $display_price !!}</span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									Thành tiền: <span id="price-main" class="price-main">{!! wc_price(($root_price * $cart_item_data['quantity']) + $price_chedobaohanh) !!}</span>
								</div>
								{{-- <div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" class="btn btn-danger btn-delete-item-cart" data-item-key="{!! $key_cart !!}" data-parent="item-{!! $key_cart !!}">Xóa item</button>
								</div> --}}
							</div>
						</div>
					@endforeach
					<div class="row">
						{{-- <table class="table table-bordered">  --}}
							{{-- <tr>
								<td colspan="1">Tổng: </td>
								<td colspan="2">{!! wc_price($total) !!}</td>
							</tr> --}}
							{{-- <tr>
								<td colspan="1">VAT (10%): </td>
								<td colspan="2">{!! wc_price(($total*0.1)) !!}</td>
							</tr> --}}
							{{-- <tr>
								<td colspan="1">Thành tiền: </td>
								<td colspan="2">{!! wc_price($total + ($total*0.1)) !!}</td>
							</tr> --}}
						{{-- </table>  --}}
					</div>
					{{-- <div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12 " style="padding-left: 0px;">
							<button type="submit" class="btn btn-danger btn-delete-all-cart" data-delete="{!! $data_delete_cart !!}">Xóa tất cả giỏ hàng</button>
						</div>
					</div> --}}
				</div>
			@else
				<div class="row">
					<p> Không có sản phẩm nào trong giỏ hàng ...</p>
				</div>
			@endif
		</div>
		<div class="muangay-info section-common">
			<div class="row title-muangay-info" style="border: 1px solid #eee; margin-bottom: 20px">
				<div class="title-review title-common" style=""><i class="fa fa-usd" aria-hidden="true"></i> 2 . PHƯƠNG THỨC MUA HÀNG
				</div>
			</div>
			<div class="row padding-section">
				<div id="tabs" class="nav nav-tabs custom-nav-bar" data-tabs="tabs">
					@foreach($phuongthucthanhtoans->posts as $key => $phuongthuc )
					<div class="col-md-4 col-sm-4 col-xs-12 tab-common">
						<a href="#phuongthuc-{!! $phuongthuc->ID !!}" data-toggle="tab"> <i class="fa @if($key == 0) fa-credit-card @elseif($key == 1) fa-truck @else fa-shopping-basket @endif" aria-hidden="true"></i> {!! $phuongthuc->post_title !!}</a>
					</div>
					@endforeach
				</div>
				<div id="muangay-tab-content" class="tab-content">
					@foreach($phuongthucthanhtoans->posts as $key => $phuongthuc )
					<div class="tab-pane @if($key == 0) active @endif" id="phuongthuc-{!! $phuongthuc->ID !!}">
						{!! $phuongthuc->post_content !!}
					</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="muangay-form-info section-common">
			<div class="row title-payment-tragop" style="border: 1px solid #eee; margin-bottom: 20px">
				<div class="title-review title-common" style=""> 3 . Điền
					thông tin mua hàng
				</div>
			</div>
			<form role="form" action="{!! site_url('wp-admin/admin-post.php') !!}" method="post">
				<input type="hidden" name="action" value="send_tragop_to_order">
				<input type="hidden" id="inp-variable-id" name="variable_id" value="{!! $variable_id !!}">
            	<input type="hidden" id="inp-product-id" name="product_id" value="{!! $product_id !!}">
            	<input type="hidden" name="name_color" value="{!! $name_color !!}">
            	<input type="hidden" name="chedobaohanh" value="{!! $chedobaohanh !!}">
            	<input type="hidden" name="loaimua" value="muangay">
				<div class="padding-section">
					<label class="title-form-payment-info">Thông tin thanh toán</label>

					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input class="form-control" type="text" name="billing_fullname" id="billing_fullname" placeholder="Nhập họ tên của bạn" required>
							<input class="form-control" type="email" name="billing_email" id="billing_email" placeholder="Email của bạn" required>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input class="form-control" type="tel" name="billing_phone" id="billing_phone" placeholder="Số điện thoại ..." required>
							<input class="form-control" type="text" name="billing_address" id="billing_address" placeholder="Số nhà / tên đường / phường xã" required>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="select-container row fixes-form">
								<div class="col-md-3 col-sm-3 col-xs-12">
									Tỉnh thành :
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12">
	                                <span class="form-group">
	                                    <input class="form-control" type="text" name="billing_provice" id="billing_provice" placeholder="Tỉnh thành ..." required>
	                                </span>
	                            </div>
							</div>
							<div class="select-container row fixes-form">
								<div class="col-md-3 col-sm-3 col-xs-12">
									Quận huyện :
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12">
	                                <span class="form-group">
	                                    <input class="form-control" type="text" name="billing_district" id="billing_district" placeholder="Quận huyện ..." required>
	                                </span>
	                            </div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-sx-12 text-center">
							<textarea name="order_comments" id="order_comments" class="form-control" rows="5" placeholder="Ghi chú thêm ..." style="resize: none"></textarea>
						</div>
					</div>
					<div class="row btn-container">
						<div class="col-md-3 col-md-offset-3 col-sm-3 col-xs-12">
							<button type="submit" class="button alt btn btn-custom btn-custom-1 " name="woocommerce_checkout_place_order" data-value="Đặt hàng" @if(empty( $woocommerce->cart->get_cart())) disabled @endif><i class="fa fa-check-circle-o pull-left" aria-hidden="true"></i> Hoàn tất
							</button>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<button class="btn btn-custom btn-custom-2"><i class="fa fa-cart-plus pull-left" aria-hidden="true"></i>
								Tiếp tục mua hàng
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
