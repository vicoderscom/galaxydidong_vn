@php
   $meta_product = $product->post;
   if(isset(get_post_meta($meta_product->ID)['san_pham_dac_biet'][0])) {
       $meta_product_special = get_post_meta($meta_product->ID)['san_pham_dac_biet'][0];
   } else {
   	   $meta_product_special = '';
   }
 // var_dump($meta_product_special);
@endphp
<div class="container">
	@include('woocommerce_template.single-product.single-product-breadcrumb')
	<div class="advertise-image">
		@php  
		if ( is_active_sidebar( 'advert-detail-product' ) ) {
			dynamic_sidebar('advert-detail-product');
		}
		@endphp
	</div>
	<div class="detail-product">
		<div class="row detail-main">
			<div class="col-md-12 col-sm-12 col-xs-12 name-product-container">
				<div class="name-product">
					@include('woocommerce_template.single-product.single-product-title')
				</div>
			</div>
			@include('woocommerce_template.single-product.single-product-image')
			<div class="col-md-8 col-sm-8 col-xs-12 detail-product-right">
				<form action="{!! site_url('wp-admin/admin-post.php') !!}" method="post" id="create-session-chedobaohanh">
				{{-- <form action="{{ site_url()."/mua-ngay" }}" method="post" id="create-session-chedobaohanh"> --}}
					<input type="hidden" name="action" value="add_to_cart">
					<input type="hidden" id="inp-variable-id" name="variable_id" value="{!! $available_variations[0]['variation_id'] !!}">
					<input type="hidden" id="inp-product-id" name="product_id" value="{!! $product->id !!}">
					<input type="hidden" id="inp-name-color" name="name_color" value="{!! $available_variations[0]['attributes']['attribute_pa_color'] !!}">
					<input type="hidden" id="inp-quantity" name="quantity" value="1">
					@include('woocommerce_template.single-product.single-product-variable', compact('available_variations'))
					@include('woocommerce_template.single-product.single-product-price')
					</form>
					<div class="row btn-detail-contain">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<button type="submit" form="create-session-chedobaohanh" name="muangay" class="btn btn-warning btn-muangay" id="btn_muangay"><i class="fa fa-shopping-cart pull-left" aria-hidden="true"></i> Mua ngay
								<p>Chọn màu - đặt mua - thanh toán tại nhà</p>
								</button>
							</div>
						</div>
						{{-- end form 1 --}}
						@if(empty($meta_product_special))
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
							<form action="{!! site_url('tra-gop') !!}" method="post" id="form-tragop">
                    			<input type="hidden" name="product-id" value="{!! $product->id !!}">
								<button type="submit" name="tragop" form="form-tragop" class="btn btn-success btn-tragop"><i class="fa fa-money pull-left" aria-hidden="true"></i> Trả góp từ  <p class="result_calc_tragop"></p>
								</button>
							</form>
							</div>
						</div>
						@endif
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 promotion-container" style="">
							<div class="promotion">
								<div class="promotion-title"><i class="fa fa-gift" aria-hidden="true"></i>
									Khuyến mãi khi mua {!! the_title() !!} tại galaxydidong.vn
								</div>
								<div class="promotion-content">
									@php echo get_field('khuyen_mai_rieng', $post->ID); @endphp
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 promotion-other">
							@include('woocommerce_template.single-product.single-product-khuyen-mai-chung')
						</div>
					</div>
			</div>
		</div>
		@include('woocommerce_template.single-product.single-product-review')
		@include('woocommerce_template.single-product.single-product-relative')

		<div class="row feature-detail">
			<div class="col-md-7 col-sm-7 col-xs-12 feature-important">
				@include('woocommerce_template.single-product.single-product-feature-image', compact('product'))
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12 feature-description">
				@include('woocommerce_template.single-product.single-product-feature-description', compact('product', 'available_variations'))
			</div>
		</div>
		
		<div class="row description-detail" style="border: 1px solid #eee; margin-bottom: 20px;">
			<div class="title-rate title-common" style="border-bottom: 1px solid #eee; padding: 5px;"><i
					class="fa fa-angle-double-right" aria-hidden="true"></i> Đánh giá chi tiết
			</div>
			<div class="col-sm-12 col-xs-12 description-content" style="padding: 15px;">
				{!! the_content() !!}
			</div>
		</div>
		@include('woocommerce_template.single-product.single-product-accessories')
		<div class="row rate-comment" style="border: 1px solid #eee; margin-bottom: 20px">
			<div class="title-rate-comment title-common" style="border-bottom: 1px solid #eee; padding: 5px;"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Đánh giá và bình luận
			</div>
			<div class="col-sm-12 col-xs-12 rate-content">
			</div>
			<div class="col-sm-12 col-xs-12 comment-content">
				<div class="fb-comments" data-href="{{the_permalink()}}" data-width="100%" data-numposts="5"></div>
			</div>
		</div>
	</div>
</div>
