@php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
$image = get_the_post_thumbnail();
@endphp
<div class="col-md-2 col-sm-2 col-xs-12 sub-item">
	<a href="{!! get_permalink() !!}">
		{!! $image !!}
		<p class="name-product-ralative">{!! get_the_title() !!}</p>
		<p class="price-product-ralative">{!! $product->get_price() !!}<sup>{!! esc_attr( get_woocommerce_currency_symbol() ) !!}</sup></p>
	</a>
	<button class="btn btn-info">So sánh</button>
</div>