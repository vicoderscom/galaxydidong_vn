@php
	global $product;
	$list_review = get_field('review_lien_quan');	

	if(!empty($list_review)) {
		foreach ($list_review as $post) {
			$review_by_ID = new WC_Product($post->ID);
			$post->title   = get_the_title($post->ID);
			$post->link       = get_permalink($post->ID);
			if(has_post_thumbnail($post->ID)) {
	           $post->image_link = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	        } else {
	           $post->image_link = get_template_directory_uri().'/assets/images/no_image.png';
	        }
		}
		$count_review = count($list_review);
	}

@endphp

@if(!empty($list_review))
	<div class="row review">
		<div class="title-review title-common" style=""><i class="fa fa-angle-double-right" aria-hidden="true"></i>
			REVIEW
		</div>
		<div class="slider-product">
			<a data-slide="prev" href="#media-review" class="hide-xs item-arrow left"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
			</a>
			<div class="col-md-12 col-sm-12 carousel-container slider_review_detail">
				<div class="carousel slide media-carousel" id="media-review">
		        	<div class="carousel-inner">
			        	@php $i = 1; @endphp
						@foreach($list_review as $key => $review)
							@php
								if($i == 1) {
									if($key == 0) {
										$active = "active";
									} else {
										$active = '';
									}
									echo '<div class="item '.$active.'"> ';
								}
							@endphp
				        		<div class="col-md-3 col-sm-3 col-xs-6 ">
									<a href="{!! $review->link !!}">
										<img class="cover_img" style="background-image: url({!! $review->image_link !!});" src="<?php echo get_template_directory_uri(); ?>/assets/images/review_product.png">
										<p class="name-news">{!! $review->title !!}</p>
									</a>
								</div>
							@php
								if($i == 4 || ($key == $count_review - 1)) {
									$i = 0;
									echo '</div> ';
								}
								$i++;
							@endphp
						@endforeach
					</div>
				</div>
			</div>
			<a data-slide="next" href="#media-review" class="hide-xs item-arrow right"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
		</div>
	</div>
@endif