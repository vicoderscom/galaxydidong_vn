@php
global $post, $product;
if ( has_post_thumbnail() ) {
	$attachment_count = count( $product->get_gallery_attachment_ids() );
	$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
	$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
	$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
		'title'	 => $props['title'],
		'alt'    => $props['alt'],
	) );

	$attachment_ids = $product->get_gallery_attachment_ids();
	$gallery_image_link = [];
	foreach( $attachment_ids as $attachment_id ) 
	{
	    $gallery_image_link[] = wp_get_attachment_url( $attachment_id );
	}

	$count_gallary = count($gallery_image_link);

} 
@endphp
<div class="col-md-4 col-sm-4 col-xs-12 detail-product-left">
	<div class="image-product">
		<div class="image-main">
			@php 
			if ( has_post_thumbnail() ) {
				echo apply_filters(
					'woocommerce_single_product_image_html',
					sprintf(
						'<img src="%s" itemprop="image" class="woocommerce-main-image zoom " id="show-image-main" title="%s" data-rel="prettyPhoto%s" >',
						esc_url( $props['url'] ),
						esc_attr( $props['caption'] ),
						$gallery,
						$image
					),
					$post->ID
				);
			} else {
				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );
			}
			@endphp
		</div>
		<div class="image-second list_image_product_detail">
			<div class="">
				<div id="thumbnail-slider" class="carousel slide">
					<div class="carousel-inner">
						@php $i = 1; @endphp
						@if(isset($gallery_image_link))
							@foreach($gallery_image_link as $key => $gallery)
								@php
									if($i == 1) {
										if($key == 0) {
											$active = "active";
										} else {
											$active = '';
										}
										echo '<div class="item '.$active.'"> ';
									}
								@endphp
									<div class="row-fluid">
										<div class="col-md-3 col-sm-3 col-xs-3 sub-item">
											<span class="click_thumbnail thumbnail"><img class="cover_img" style="background-image: url({!! $gallery !!});" alt="Image" src="<?php echo get_template_directory_uri(); ?>/dist/images/tranparent.png"/></span>
										</div>
									</div>
								@php
								if($i == 4 || ($key == $count_gallary - 1)) {
									$i = 0;
									echo '</div> ';
								}
								$i++;
							@endphp
							@endforeach
						@endif
					</div>
					<div class="arrow-container hide-xs">
						<a class="left carousel-control" href="#thumbnail-slider" data-slide="prev"><i
								class="fa fa-angle-left" aria-hidden="true"></i></a>
					</div>
					<div class="arrow-container hide-xs">
						<a class="right carousel-control" href="#thumbnail-slider" data-slide="next"><i
								class="fa fa-angle-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>