@php
	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	if ( ! empty( $breadcrumb ) ) {

		echo $wrap_before;
		echo '<div class="row breadcrumb" style="">';
		foreach ( $breadcrumb as $key => $crumb ) {

			echo $before;

			if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
				echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
			} else {
				echo esc_html( $crumb[0] );
			}

			echo $after;

			if ( sizeof( $breadcrumb ) !== $key + 1 ) {
				echo $delimiter;
			}

		}
		echo '</div>';
		echo $wrap_after;

	}
@endphp