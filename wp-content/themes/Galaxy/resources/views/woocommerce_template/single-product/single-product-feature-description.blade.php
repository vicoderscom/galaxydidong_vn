@php
	$camera = get_field('acf_camera', $product->post->ID);
	$bonhotrong = get_field('acf_bonhotrong', $product->post->ID);
	$manhinh = get_field('acf_manhinh', $product->post->ID);
	$ram = get_field('acf_ram', $product->post->ID);
	$hedieuhanh = get_field('acf_hedieuhanh', $product->post->ID);
	$pin = get_field('acf_pin', $product->post->ID);
	$cpu = get_field('acf_cpu', $product->post->ID);

	$id = $product->post->ID;
	$man_hinh_cam_ung = get_field('man_hinh_cam_ung', $id);
	$mat_do_diem_anh = get_field('mat_do_diem_anh', $id);
	$kich_thuoc_khau_do = get_field('kich_thuoc_khau_do', $id);
	$may_quay_video = get_field('may_quay_video', $id);
	$bluetooth = get_field('bluetooth', $id);
	$ket_noi = get_field('ket_noi', $id);
	$usb = get_field('usb', $id);
	$wifi = get_field('wifi', $id);
	$cac_tinh_nang = get_field('cac_tinh_nang', $id);
	$cong_nghe = get_field('cong_nghe', $id);
	$kich_thuoc_man_hinh = get_field('kich_thuoc_man_hinh', $id);
	$man_hinh_cam_ung = get_field('man_hinh_cam_ung', $id);
	// $mat_do_diem_anh = get_field('mat_do_diem_anh', $id);
	$so_luong_mau = get_field('so_luong_mau', $id);
	$do_phan_giai = get_field('do_phan_giai', $id);
	// $bo_nho = get_field('bo_nho', $id);
	// $bo_nho_he_thong = get_field('bo_nho_he_thong', $id);
	$bo_xu_ly = get_field('bo_xu_ly', $id);
	$chip_he_thong = get_field('chip_he_thong', $id);
	$mo_rong_luu_tru = get_field('mo_rong_luu_tru', $id);
	$xu_ly_do_hoa = get_field('xu_ly_do_hoa', $id);
	$kich_thuoc_vat_ly = get_field('kich_thuoc_vat_ly', $id);
	$trong_luong = get_field('trong_luong', $id);
	$cam_bien = get_field('cam_bien', $id);
	$thong_bao = get_field('thong_bao', $id);
	$dich_vu_truc_tuyen = get_field('dich_vu_truc_tuyen', $id);
	$am_thanh_va_loa = get_field('am_thanh_va_loa', $id);
	$youtube_player = get_field('youtube_player', $id);
	$khac = get_field('khac', $id);

@endphp
@if(empty($meta_product_special))
<div class="feature-text title-common"><i class="fa fa-info-circle" aria-hidden="true"></i> Thông số kỹ thuật
</div>
<div class="col-md-12 col-sm-12 col-xs-12 feature-item">
	<p class="name-product-ralative">
		<table class="table">
			<tr>
				<td>Màu sắc: </td>
				<td colspan="2">
					@foreach($available_variations as $key => $ele_vari)
						@if($key != 0) 
						{!! ', ' !!}
						@endif
						<span>{!! $ele_vari['attributes']['attribute_pa_color'] !!}</span>
					@endforeach
				</td>
			</tr>
			<tr>
				<td>CPU: </td>
				<td colspan="2">
					{!! $cpu !!}
				</td>
			</tr>
			<tr>
				<td>Camera: </td>
				<td colspan="2">
					{!! $camera !!}
				</td>
			</tr>
			<tr>
				<td>Bộ nhớ trong: </td>
				<td colspan="2">
					{!! $bonhotrong !!}
				</td>
			</tr>
			<tr>
				<td>Màn hình: </td>
				<td colspan="2">
					{!! $manhinh !!}
				</td>
			</tr>
			<tr>
				<td>Hệ điều hành: </td>
				<td colspan="2">
					{!! $hedieuhanh !!}
				</td>
			</tr>
			<tr>
				<td>Ram: </td>
				<td colspan="2">
					{!! $ram !!}
				</td>
			</tr>
			<tr>
				<td>Pin: </td>
				<td colspan="2">
					{!! $pin !!}
				</td>
			</tr>
		</table>
	</p>
	<div class="feature-desciption-more text-center">
		<button type="button" data-toggle="modal" data-target="#detail-properties-product" class="btn btn-info custom-btn-description">Xem cấu hình chi tiết</button>
		<div class="modal fade" role="dialog" id="detail-properties-product">
			<div class="modal-dialog">
				<div class="modal-content">
				    <div class="clone_modal_product_detail">
				    	<button class="btn btn-danger pull-right" data-dismiss="modal">Đóng </button>
				    </div>
					<div class="modal-body">
						<p class="title-modal" style="font-size: 40px">Thông tin cấu hình chi tiết</p>
						<table class="table" style="">
							<tr>
								<td>Màu sắc: </td>
								<td colspan="2">
									@foreach($available_variations as $key => $ele_vari)
										@if($key != 0) 
										{!! ', ' !!}
										@endif
										<span>{!! $ele_vari['attributes']['attribute_pa_color'] !!}</span>
									@endforeach
								</td>
							</tr>
							<tr>
								<td>CPU: </td>
								<td colspan="2">
									{!! $cpu !!}
								</td>
							</tr>
							<tr>
								<td>Camera: </td>
								<td colspan="2">
									{!! $camera !!}
								</td>
							</tr>
							<tr>
								<td>Bộ nhớ trong: </td>
								<td colspan="2">
									{!! $bonhotrong !!}
								</td>
							</tr>
							<tr>
								<td>Màn hình: </td>
								<td colspan="2">
									{!! $manhinh !!}
								</td>
							</tr>
							<tr>
								<td>Hệ điều hành: </td>
								<td colspan="2">
									{!! $hedieuhanh !!}
								</td>
							</tr>
							<tr>
								<td>Ram: </td>
								<td colspan="2">
									{!! $ram !!}
								</td>
							</tr>
							<tr>
								<td>Pin: </td>
								<td colspan="2">
									{!! $pin !!}
								</td>
							</tr>
							<tr>
								<td>Màn hình cảm ứng: </td>
								<td colspan="2">
									{!! (!empty($man_hinh_cam_ung) ? $man_hinh_cam_ung : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Mật độ điểm ảnh: </td>
								<td colspan="2">
									{!! (!empty($mat_do_diem_anh) ? $mat_do_diem_anh : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Kích thước khẩu độ: </td>
								<td colspan="2">
									{!! (!empty($kich_thuoc_khau_do) ? $kich_thuoc_khau_do : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Máy quay video: </td>
								<td colspan="2">
									{!! (!empty($may_quay_video) ? $may_quay_video : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Bluetooth: </td>
								<td colspan="2">
									{!! (!empty($bluetooth) ? $bluetooth : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Kết nối: </td>
								<td colspan="2">
									{!! (!empty($ket_noi) ? $ket_noi : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>USB: </td>
								<td colspan="2">
									{!! (!empty($usb) ? $usb : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Wifi: </td>
								<td colspan="2">
									{!! (!empty($wifi) ? $wifi : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Các tính năng: </td>
								<td colspan="2">
									{!! (!empty($cac_tinh_nang) ? $cac_tinh_nang : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Công nghệ: </td>
								<td colspan="2">
									{!! (!empty($cong_nghe) ? $cong_nghe : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Kích thước màn hình: </td>
								<td colspan="2">
									{!! (!empty($kich_thuoc_man_hinh) ? $kich_thuoc_man_hinh : 'Đang cập nhật') !!}
								</td>
							</tr>
							{{-- <tr>
								<td>Màn hình cảm ứng: </td>
								<td colspan="2">
									{!! (!empty($man_hinh_cam_ung) ? $man_hinh_cam_ung : 'Đang cập nhật') !!}
								</td>
							</tr> --}}
							<tr>
								<td>Mật đô điểm ảnh: </td>
								<td colspan="2">
									{!! (!empty($mat_do_diem_anh) ? $mat_do_diem_anh : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Số lượng màu: </td>
								<td colspan="2">
									{!! (!empty($so_luong_mau) ? $so_luong_mau : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Độ phân giải: </td>
								<td colspan="2">
									{!! (!empty($do_phan_giai) ? $do_phan_giai : 'Đang cập nhật') !!}
								</td>
							</tr>
							{{-- <tr>
								<td>Bộ nhớ: </td>
								<td colspan="2">
									{!! (!empty($bo_nho) ? $bo_nho : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Bộ nhớ hệ thống: </td>
								<td colspan="2">
									{!! (!empty($bo_nho_he_thong) ? $bo_nho_he_thong : 'Đang cập nhật') !!}
								</td>
							</tr> --}}
							<tr>
								<td>Bộ xử lý: </td>
								<td colspan="2">
									{!! (!empty($bo_xu_ly) ? $bo_xu_ly : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Chip hệ thống: </td>
								<td colspan="2">
									{!! (!empty($chip_he_thong) ? $chip_he_thong : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Mở rộng lưu trữ: </td>
								<td colspan="2">
									{!! (!empty($mo_rong_luu_tru) ? $mo_rong_luu_tru : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Xử lý đồ họa: </td>
								<td colspan="2">
									{!! (!empty($xu_ly_do_hoa) ? $xu_ly_do_hoa : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Kích thước vật lý: </td>
								<td colspan="2">
									{!! (!empty($kich_thuoc_vat_ly) ? $kich_thuoc_vat_ly : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Trọng lượng: </td>
								<td colspan="2">
									{!! (!empty($trong_luong) ? $trong_luong : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Cảm biến: </td>
								<td colspan="2">
									{!! (!empty($cam_bien) ? $cam_bien : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Thông báo: </td>
								<td colspan="2">
									{!! (!empty($thong_bao) ? $thong_bao : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Dịch vụ trực tuyến: </td>
								<td colspan="2">
									{!! (!empty($dich_vu_truc_tuyen) ? $dich_vu_truc_tuyen : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Âm thanh và Loa: </td>
								<td colspan="2">
									{!! (!empty($am_thanh_va_loa) ? $am_thanh_va_loa : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Youtube Player: </td>
								<td colspan="2">
									{!! (!empty($youtube_player) ? $youtube_player : 'Đang cập nhật') !!}
								</td>
							</tr>
							<tr>
								<td>Khác: </td>
								<td colspan="2">
									{!! (!empty($khac) ? $khac : 'Đang cập nhật') !!}
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif