@php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $woocommerce_loop;
$posts_per_page = 12;
$orderby = 'ID';

if ( empty( $product ) || ! $product->exists() ) {
	return;
}

if ( ! $related = $product->get_related( $posts_per_page ) ) {
	return;
}

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->id )
) );

$products = new WP_Query( $args );
foreach ($products->posts as $post) {
	$product_by_ID = new WC_Product($post->ID);
	$post->title   = get_the_title($post->ID); 
	$post->link       = get_permalink($post->ID);
	if(has_post_thumbnail($post->ID)) {
        $post->image_link = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	} else {
		$post->image_link = get_template_directory_uri().'/assets/images/no_image_2.png';
	}
    $post->price      = wc_price($product_by_ID->get_price());
}

$count_pro_related = count($products->posts);

@endphp

@if ( $products->have_posts() )
<div class="row relative-product product_detail slider_product_involve">
	<div class="title-relative title-common"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sản phẩm tương tự
	</div>
	@php woocommerce_product_loop_start(); @endphp
	<div class="slider-product" style="">
		<a data-slide="prev" href="#media-relative" class="hide-xs item-arrow left"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
		</a>
		<div class="col-md-12 col-sm-12">
			<div class="carousel slide media-carousel" id="media-relative">
	        	<div class="carousel-inner">
	        		@php 
	        		    $i = 1; 
	        		    $list_product = [];
	        		@endphp
					@foreach ($products->posts as $key => $product)
						@php
						    $list_product[] = $product->ID;
							if($i == 1) {
								if($key == 0) {
									$active = "active";
								} else {
									$active = '';
								}
								echo '<div class="item '.$active.'"> ';
							}
						@endphp
						<div class="col-md-2 col-sm-2 col-xs-6 product_involve">
							<a href="{!! $product->link !!}">
								<img class="cover_img" style="background-image: url({!! $product->image_link !!})" src="<?php echo get_template_directory_uri(); ?>/dist/images/tranparent.png">
								<p class="name-product-ralative">{!! $product->title !!}</p>
								<p class="price-product-ralative">{!! $product->price !!}</p>
							</a>
							{{-- <button class="btn btn-info"></button> --}}
							{{-- <div class="model_compare">
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal_compare">So sánh</button>
							</div> --}}
						</div>	
						
						@php
							if($i == 6 || ($key == $count_pro_related - 1)) {
								$i = 0;
								echo '</div> ';
							}
							$i++;
						@endphp
					@endforeach
					@php
					    // var_dump($list_product);
					@endphp
					{{-- <div id="myModal_compare" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <!-- Modal content-->
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Modal Header</h4>
					      </div>
					      <div class="modal-body">
					         <div class="container">
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					    </div>

					  </div>
					</div> --}}
				</div>
			</div>
		</div>
		<a data-slide="prev" href="#media-relative" class="hide-xs item-arrow right"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
	</div>
	@php woocommerce_product_loop_end(); @endphp
</div>
@endif
@php wp_reset_query() @endphp
