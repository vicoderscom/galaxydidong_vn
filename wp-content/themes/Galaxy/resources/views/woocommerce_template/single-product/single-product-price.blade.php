@php
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	global $product;

@endphp
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 price-main" data-price-calc="{!! $product->get_price() !!}"><span class="get_price" data-price="{!! $product->get_price() !!}">{!! wc_price($product->get_price()) !!}</span><span class="extent-price"></span></div>
</div>

