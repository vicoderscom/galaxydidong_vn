@php
	$images = get_field('gallery_image', $product->post->ID);
	$count_image = count($images);
@endphp
@if(empty($meta_product_special))
<div class="feature-image title-common"><i class="fa fa-hand-peace-o" aria-hidden="true"></i> Đặc điểm nổi bật
</div>
<div class="col-md-12 col-sm-12 col-xs-12 feature-item">
	<a data-slide="prev" href="#carousel-feature-detail" class="hide-xs item-arrow left"><i class="fa fa-arrow-left" aria-hidden="true"></i>
	</a>
	<div class="col-md-12 col-sm-12 carousel-container">
		<div id="carousel-feature-detail" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
			@foreach($images as $key => $image)
				<div class="item @if($key == 0) active @endif">
					<img src="{!! $image['url'] !!}" width="100%">
				</div>
			@endforeach
			</div>
		</div>
	</div>
	<a data-slide="next" href="#carousel-feature-detail" class="hide-xs item-arrow right"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
</div>
@endif