@php
	$args = [
		'post_type' => 'khuyenmaichung',
		'post_status' => 'publish',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderBy' => 'ID'
	];
	$khuyenmais = new WP_Query( $args );	
@endphp
<div class="" style="color: #c0392b;">
	@php
		while ( $khuyenmais->have_posts() ) : $khuyenmais->the_post();
			esc_html(the_content());
		endwhile;
	@endphp
</div>
@php wp_reset_postdata(); @endphp