@php
	$args = [
		'post_type' => 'chedobaohanh',
		'post_status' => 'publish',
		'order' => 'ASC',
		'orderBy' => 'ID'
	];
	$baohanhs = new WP_Query( $args );	
@endphp
@if(isset($meta_product_special) && !empty($meta_product_special))
    <style>
    	.color_filter_product {
    		display:none;
    		visibility: hidden;
    	}
    </style>
@endif
@php
	$array_color = ['black', 'white', 'blue', 'green', 'grey', 'pink', 'red', 'yellow'];	
@endphp

<div class="row">
	<div class="col-md-5 col-sm-5 col-xs-12 color_filter_product">
		<div class="choose-color">
			<span>Chọn màu: </span>
			<ul id="select-color" style="display: inline-block; padding-left: 10px; margin: 0;">
				@foreach($available_variations as $key => $ele_vari)
					@php
						if(in_array($ele_vari['attributes']['attribute_pa_color'], $array_color)) {
							$check_color = true;
						} else {
							$check_color = false;
						}
					@endphp
					<li 
						data-variable="{!! $ele_vari['variation_id'] !!}"
						data-color="{!! $ele_vari['attributes']['attribute_pa_color'] !!}" 
						data-price="{{ $ele_vari['display_price'] }}" 
						@if(!empty($ele_vari['image_src'])) data-img-src=" {{ $ele_vari['image_src'] }} " @endif 
						data-price-calc="{!! $ele_vari['display_price'] !!}"
						class="li-select-color @if($key == 0) active @endif @if(!$check_color){{ 'price_other' }}@endif" 
						style="background-color: @if($ele_vari['attributes']['attribute_pa_color'] != 'white') {!! $ele_vari['attributes']['attribute_pa_color'] !!} @else {!! '#E8E8EA' !!} @endif; "
					>
					@if(!$check_color)
						@php
							$meta = get_post_meta($ele_vari['variation_id'], 'attribute_pa_color', true);
							$term = get_term_by('slug', $meta, 'pa_color');
							echo $term->name;
						@endphp
					@endif
					</li>
				@endforeach
			</ul>
		</div>
	</div>
	@if(empty($meta_product_special))
	<div class="col-md-7 col-sm-7 col-xs-12">
		<div class="guarantee-container">Bảo hành:
            <span class="form-group">
                <select name="chedobaohanh" id="pa_chedobaohanh" required>
                	<option value="0" data-price-baohanh="0">-- chọn gói bảo hành --</option>
                @foreach ($baohanhs->posts as $key => $baohanh) {
						<option value="{!! ($baohanh->ID) !!}" data-price-baohanh="{!! (get_field('gia_tien', $baohanh->ID)) !!}">{!! ($baohanh->post_title) !!}</option>
				@endforeach
                </select>
                <a href="<?php echo site_url('chuyen-muc/che-do-bao-hanh'); ?>"><i class="fa fa-info-circle" aria-hidden="true" style="color:#E88834;"></i></a>
            </span>
		</div>
	</div>
	@endif
</div>

@php wp_reset_postdata(); @endphp