@php
global $product;

$sanPhamCoBigImageKhong = get_field("san_pham_co_anh_lon_khong");
@endphp
<div class="motsanpham  @php  if ($sanPhamCoBigImageKhong == "co") {
    echo "sanpham-big";
} ; @endphp">

    

        <a href="@php  the_permalink(); @endphp">
            <div class="thongtin-hienra-khi-hover" style="z-index: 2; overflow: hidden;">
                <p> CPU: @php echo get_field("acf_cpu"); @endphp</p>

                <p>Ram: @php echo get_field("acf_ram"); @endphp</p>

                <p>Bộ nhớ trong: @php echo get_field("acf_bonhotrong"); @endphp</p>

                <p>Màn Hình: @php echo get_field("acf_manhinh"); @endphp</p>

                <p>Camera: @php echo get_field("acf_camera"); @endphp</p>

                <p>OS: @php echo get_field("acf_hedieuhanh"); @endphp</p>

                <p>Pin: @php echo get_field("acf_pin"); @endphp</p>
            </div>
        </a>

        <div class="inside-motsanpham">
            @php
            if($sanPhamCoBigImageKhong == "khong" || empty($sanPhamCoBigImageKhong)): 
            @endphp

                <div class="anhsanpham">
                    @php
                        if(has_post_thumbnail()) {
                            $images = wp_get_attachment_url(get_post_thumbnail_id());
                        } else {
                            $images = get_template_directory_uri().'/assets/images/no_image_2.png';
                        }
                       $loai = get_field('loai');
                    @endphp
                    <img class="cover_img" style="background-image: url({{$images}})" src="{{ get_template_directory_uri() }}/dist/images/tranparent.png" alt="">
                    @if($loai == 'hot')
                        <img class="cover_img" style="position: absolute; width: 80px !important; top: 0; right: 0; z-index: 1; background-image: url('{!! get_template_directory_uri().'/dist/images/hot.png'; !!}')" src="{{ get_template_directory_uri() }}/dist/images/tranparent.png" alt="">
                    @elseif($loai == 'moi')
                        <img class="cover_img" style="position: absolute; width: 80px !important; top: 0; right: 0; z-index: 1; background-image: url('{!! get_template_directory_uri().'/dist/images/moi.png'; !!}')" src="{{ get_template_directory_uri() }}/dist/images/tranparent.png" alt="">
                    @elseif($loai == 'tragop')
                        <img class="cover_img" style="position: absolute; width: 80px !important; top: 0; right: 0; z-index: 1; background-image: url('{!! get_template_directory_uri().'/dist/images/tragop.png'; !!}')" src="{{ get_template_directory_uri() }}/dist/images/tranparent.png" alt="">
                    @endif
                </div>
                <div class="tieude-sanpham">
                    <a href="@php the_permalink(); @endphp"> @php the_title(); @endphp </a>
                </div>

                <div class="gia-sanpham" data-price="{!! $product->get_price() !!}">
                    {!! wc_price($product->get_price()) !!}
                </div>

                <div class="tragop">
                    Trả góp từ: <span class="data-change-price">@php echo wc_price(($product->get_price() - ($product->get_price() *0.3))/6); @endphp</span>
                </div>

                <div class="row two-button">
                    <div class="col-md-6 col-sm-6 col-xs-6 pull-left">
                        <a href="@php  the_permalink(); @endphp"><button style="border: unset;" class="btn btn-primary btn-muangay">XEM NGAY</button>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                        <form action="{!! site_url('tra-gop') !!}" method="post">
                            <input type="hidden" name="product-id" value="{!! $product->id !!}">
                            <button class="btn btn-success btn-tragop" type="submit">
                                TRẢ GÓP
                            </button>
                        </form>
                    </div>
                </div>
            @php
            endif;
            if( isset($sanPhamCoBigImageKhong) && $sanPhamCoBigImageKhong == "co"): 
            @endphp
                <a href="@php  the_permalink(); @endphp">
                    <div class="anhsanphambig">
                        @php
                           if(has_post_thumbnail()) {
                               $images = wp_get_attachment_url(get_post_thumbnail_id());
                           } else {
                               $images = get_template_directory_uri().'/assets/images/no_image.png';
                           }
                        @endphp
                        <img class="cover_img" style="background-image: url({{$images}}); background-position: initial;" src="{{ get_template_directory_uri() }}/assets/images/home_trans.png" alt="">
                    </div>
                </a>
                <div class="row row-sanpham" style="position: absolute;bottom: 0;width: 100%;">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="tieude-sanpham">
                            <a href="@php the_permalink(); @endphp"> @php the_title(); @endphp </a>
                        </div>

                        <div class="gia-sanpham" data-price="{!! $product->get_price() !!}">
                            {!! wc_price($product->get_price()) !!}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row two-button">
                            <div class="col-md-6 col-sm-6 col-xs-6 pull-left">
                                <a href="@php  the_permalink(); @endphp"><button style="border: unset;" class="btn btn-primary btn-muangay">XEM NGAY</button>
                                </a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                                <form action="{!! site_url('tra-gop') !!}" method="post">
                                    <input type="hidden" name="product-id" value="{!! $product->id !!}">
                                    <button class="btn btn-success btn-tragop" type="submit">
                                        TRẢ GÓP
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @php
            endif;
            @endphp

    
        </div>

</div>

