@php
global $post, $wp_query;

$request_uri = $_SERVER['REQUEST_URI'];
$explode_uri = explode( '/', $request_uri);


$len = count($explode_uri);

if(empty($explode_uri) || empty($explode_uri[$len - 2])) {
    return "Không tìm thấy chuyên mục này !";
}
$slug = $explode_uri[$len - 2];

$get_all_cate = get_terms('category');

$arr_slug_cate = [];
if(empty($get_all_cate)){
    return 'Không tồn tại tin tức nào !';
}
foreach ($get_all_cate as $key_cate => $cate) {
    $arr_slug_cate[] = $cate->slug;
}
if(!in_array($slug, $arr_slug_cate)) {
    return "Không tìm thấy chuyên mục này !";
}

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$args = [
    'post_type' => 'chedobaohanh',
    'posts_per_page' => 10,
    'post_status' => ['publish'],
    'paged' => $paged,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms'    => $slug,
        )
    )
];
$tintuc = new WP_Query($args);
$total_pages = $tintuc->max_num_pages;

$term_by = get_term_by('slug', $slug, 'category');
$name_term = $term_by->name;

// echo "<pre>";
// var_dump($tintuc); die;
@endphp
<section id="list-post-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <?php do_action('woocommerce_before_main_content');?></p>
            </div>
        </div>
        <div class="row">
            <div class="list-post col-md-8 col-sm-8 col-xs-12">
                <div class="heading">
                    <p>{!! $name_term !!}</p>
                </div>
                <div class="content">
                    @foreach($tintuc->posts as $key => $tin)
                    <div class="row row-post">
                        <div class="col-md-10 col-sm-10 col-xs-12 col-left" style="padding-left:0">
                            <a href="@php the_permalink($tin->ID) @endphp"><div class="title">{!! $tin->post_title !!}</div></a>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 col-right">
                            <div class="row pull-right xem-tiep"><a href="@php the_permalink($tin->ID) @endphp"">Xem chi tiết ...</a></div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
                <div class="row">
                    <div class="paginate pull-right">
                        @php
                            do_action('garung_paginate', $paged, $total_pages);
                        @endphp 
                    </div>
                </div>
            </div>
            <div class="sidebar col-md-4 col-sm-4 col-xs-12 ">
                @php
                if (is_active_sidebar('siderbar-news')) {
                    dynamic_sidebar('siderbar-news');
                }
                @endphp
            </div>
        </div>
    </div>
</section>