<div class="noidungtrang container" id="success-order-page">
	<div class="row">
		<div class="tieude-trang text-success">
			Bạn đã đặt hàng thành công
		</div>
		<div class="subtitle-trang">
			Chúng tôi đã nhận được đơn hàng của bạn. Hãy chờ nhân viên tư vấn liên lạc lại với bạn.
		</div>
	</div>

	<div class="row thongtinkhachhang">
		<div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-2 col-md-offset-2 text-center">
			<img src="{!! asset('images/icon-thanhtoanthanhcong.png') !!}">
		</div>

		<div class="col-xs-12 col-sm-7 col-md-5 col-md-offset-3 text-justify">
			<p><strong>Mã hóa đơn: </strong> {!! $order_id !!}</p>
			<p><strong>Tên khách hàng: </strong> {!! $post_data['billing_fullname'] !!}</p>

			<p><strong>SĐT:</strong> {!! $post_data['billing_phone'] !!}</p>

			<p><strong>Email:</strong> {!! $post_data['billing_email'] !!}</p>

			<p><strong>Ngày mua:</strong> {!! $datetime_now !!}</p>

			<p><strong>Trạng thái:</strong> Đang xử lý</p>
		</div>
	</div>

	<div class="row bangthongtin">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>Tên sản phẩm</th>
				<th>Số lượng</th>
				<th>Giá</th>
				<th>Thành tiền</th>
			</tr>
			</thead>
			<tbody>
			@php
			if($get_cart):
				$total = 0;
				$subtotal = 0;
				foreach ( $get_cart as $cart_item_key => $values ):
	                $custom_values = $values['wdm_user_custom_data_value']['data'];
	                $variable_id = $values['wdm_user_custom_data_value']['variable_id'];
	                $root_price = $custom_values['root_price'];
	                $price = $custom_values['calc_price'];
	                $title = $custom_values['title'];
	                $color = $custom_values['color'];
	                $title_chedobaohanh = $custom_values['title_chedobaohanh'];
	                $price_chedobaohanh = $custom_values['price_chedobaohanh'];

	                $subtotal = $price*$values['quantity'];
	                $total += $subtotal;
	        @endphp
				<tr>
					<td>
						<p>{!! $title !!}</p>

						<p>Màu sắc: {!! $color !!}</p>
						@if(!empty($title_chedobaohanh))
							<p>Bảo hành: {!! ($title_chedobaohanh) !!} </p>
							@if(!empty($custom_values['loaimua']))
								<p>Giá tiền gói bảo hành: {!! ($price_chedobaohanh) !!}</p>
							@else
								<p>Giá tiền gói bảo hành: {!! wc_price($price_chedobaohanh) !!}</p>
							@endif
						@endif
						@if(!empty($custom_values['tratruoc']))
							<p>Trả trước: {!! $custom_values['tratruoc'] !!}</p>
						@endif
						@if(!empty($custom_values['hantratruoc']))
							<p>Hạn trả: {!! $custom_values['hantratruoc'] !!}</p>
						@endif
						@if(!empty($custom_values['tientratruoc']))
							<p>Tiền trả trước: {!! $custom_values['tientratruoc'] !!}</p>
						@endif
						@if(!empty($custom_values['trahangthang']))
							<p>Trả hàng tháng: {!! $custom_values['trahangthang'] !!}</p>
						@else

						@endif
					</td>
					<td>{!! $values['quantity'] !!}</td>
					<td>{!! wc_price($root_price) !!}</td>
					<td>{!! wc_price($subtotal) !!}</td>
				</tr>
	        @php
            	endforeach;
            endif;
			@endphp

			<tr>
				<td class="total" colspan="4"> Tổng cộng: <span> {!! wc_price($total) !!} </span></td>
			</tr>

			</tbody>
		</table>
	</div>


	<div class="row">

	</div>
</div>