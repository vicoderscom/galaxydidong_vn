<!DOCTYPE html>
<html <?php language_attributes();?> style="margin-top:0 !important;">
<head>
  <title><?php wp_title(); ?></title>
  <base href="<?php echo site_url(); ?>">
  <meta charset="<?php bloginfo('charset');?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="<?php echo get_template_directory_uri().'/dist/images/favicon17x17.ico'; ?>" type="image/x-icon">
  <link rel="icon" href="<?php echo get_template_directory_uri().'/dist/images/favicon17x17.ico'; ?>" type="image/x-icon">
  <script type="text/javascript" >
      var SITE_URL = "<?php echo site_url(); ?>";
      var ajaxurl  = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
  </script>

    

  <?php wp_head();?>
  <script type="text/javascript">
      var currency_symbol = "<?php echo get_woocommerce_currency_symbol(); ?>";

      function format_price(price) {
        price = Math.round(price);
        var str_price = price.toString();
        var i = 0, len = str_price.length, dem = 1;
        var str = '';
        for(i = len - 1; i >= 0; i--){
          if(dem == 3 && i !== 0) {
            str = '.' + str_price[i] + str;
            dem = 0;
          } else {
              str = str_price[i] + str;
          }
          dem++;
        }
        return str;
      }
  </script>
</head>        
<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5c47fe3651410568a107e8e2/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
<body>

<?php

$logo = get_option('galaxy_upload_logo');
if (!empty($logo['inp_logo'])) {
    $logo_upload = $logo['inp_logo'];
} elseif (!empty($logo['url_logo'])) {
    $logo_upload = $logo['url_logo'];
} else {
    $logo_upload = "http://fakeimg.pl/174x68/";
}
echo view('_patials.header', compact('logo_upload'))?>
