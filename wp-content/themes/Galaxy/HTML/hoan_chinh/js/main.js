$(document).ready(function () {


    var videowidth = $(".video-container").width();
    var videoheight = videowidth / 1.65


    $(window).on('resize', function () {
        var videowidth = $(".video-container").width();
        var videoheight = videowidth / 1.65

        $(".video-container").css("height", videoheight)
    });

    $(".video-container").css("height", videoheight)

    $(window).scroll(function () {

        var distance = $(document).height() - $(document).scrollTop();
        console.log(distance);
        if (distance > 1400) {
            $(".thanhlienhe").fadeIn()

        } else {
            $(".thanhlienhe").fadeOut()
        }

        var videowidth = $(".video-container").width();
        var videoheight = videowidth / 1.65

        $(".video-container").css("height", videoheight)
    });


    $("#menuu").mmenu();
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });


    $('.outer').slick({
        speed: 500,
        fade: true,
        cssEase: 'linear',
        dots: true,
        arrows: false,
        customPaging: function (slider, i) {
            var thumb = $(slider.$slides[i]).data('thumb');
            return '<div style="background-color: ' + thumb + '" class="custompaging">' + i + '</div>';
        },
    });

})