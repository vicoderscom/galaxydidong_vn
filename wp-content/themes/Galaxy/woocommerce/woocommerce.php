<?php
global $woocommerce;
class customize_woocomerce
{
    public $woocommerce;
    public function __construct()
    {
        global $woocommerce;
        $this->woocommerce = $woocommerce;
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);

        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

        add_action('woocommerce_single_product_summary', [$this, 'custom_single_product'], 11);

        //========================= custom form with admin post ======================
        add_action('admin_post_add_to_cart', [$this, 'custom_add_to_cart']);
        add_action( 'admin_post_nopriv_add_to_cart', [$this, 'custom_add_to_cart'] );

        add_action('admin_post_send_tragop_to_order', [$this, 'create_order_for_tragop']);
        add_action( 'admin_post_nopriv_send_tragop_to_order', [$this, 'create_order_for_tragop'] );

        add_action('admin_post_custom_create_order', [$this, 'garung_custom_create_order']);
        //============================================================================
        // add_action('wp_ajax_wdm_add_user_custom_data_options', 'wdm_add_user_custom_data_options_callback');
        // add_action('wp_ajax_nopriv_wdm_add_user_custom_data_options', 'wdm_add_user_custom_data_options_callback');

        add_filter('woocommerce_add_cart_item_data', [$this, 'garung_add_item_data'],1,2);
        add_filter('woocommerce_get_cart_item_from_session', [$this, 'garung_get_cart_items_from_session'], 1, 3);
        // add_action('woocommerce_add_order_item_meta', [$this, 'garung_add_values_to_order_item_meta'],1,2);
        add_action('woocommerce_before_cart_item_quantity_zero','garung_remove_user_custom_data_options_from_cart',1,1);

        //================ for delete item on cart via ajax =====================
        add_action( 'wp_ajax_delete_item_cart', [$this, 'garung_delete_item_cart_by_ajax'] );
        add_action( 'wp_ajax_nopriv_delete_item_cart', [$this, 'garung_delete_item_cart_by_ajax'] );
        add_action( 'wp_ajax_delete_all_cart', [$this, 'garung_delete_all_cart'] );
        add_action( 'wp_ajax_nopriv_delete_item_cart', [$this, 'garung_delete_all_cart'] );
    }

    public function custom_single_product()
    {
        echo view('woocommerce_template.single-detail');
    }

    public function custom_add_to_cart()
    {
        // session_start();

        if (empty($_POST['product_id'])) {
            wp_redirect(site_url('/'));
        }

        if (empty($_POST['variable_id'])) {
            wp_redirect(site_url('/'));
        }

        if (empty($_POST['quantity']) || $_POST['quantity'] <= 0 || ($_POST['quantity'] >= 100)) {
            $_POST['quantity'] = 1;
        }

        if (empty($_POST['name_color'])) {
            wp_redirect(site_url('/'));
        }

        $product = new WC_Product($_POST['product_id']);
        $variable_product = new WC_Product($_POST['variable_id']);

        $chedobaohanh_price = abs(get_field('gia_tien', $_POST['chedobaohanh']));

        if(empty($chedobaohanh_price)) {
            $chedobaohanh_price = 0;
            $data['root_price'] = (float) ($variable_product->get_price());
            $data['calc_price'] = (float) ($variable_product->get_price());
            $data['display_price'] = wc_price($variable_product->get_price());
        } else {
            $data['root_price'] = (float) ($variable_product->get_price());
            $data['calc_price'] = (float) ($variable_product->get_price()) + (float) ($chedobaohanh_price);
            $data['display_price'] = wc_price($variable_product->get_price()) . " + " . (wc_price((float) $chedobaohanh_price));
        }

        $chedobaohanh = get_post($_POST['chedobaohanh']);
        if(!empty($chedobaohanh)){
            $data['title_chedobaohanh'] = $chedobaohanh->post_title;
            $data['price_chedobaohanh'] = $chedobaohanh_price;
        } else {
            $data['title_chedobaohanh'] = '';
            $data['price_chedobaohanh'] = '';
        }

        $data['title'] = $product->post->post_title;
        $data['image_link'] = wp_get_attachment_url(get_post_thumbnail_id($_POST['variable_id']));
        $data['color'] = $_POST['name_color'];


        $cart_item_data = [
            'product_id' => $_POST['product_id'],
            'variable_id' => $_POST['variable_id'],
            'variation'   => $variable_product,
            'quantity'    => $_POST['quantity'],
            'chedobaohanh' => $_POST['chedobaohanh'],
            'data' => $data,
        ];
        // var_dump($data); die;

        $_SESSION['wdm_user_custom_data'] = $cart_item_data;

        exit(wp_redirect(site_url('mua-ngay')));
    }

    public function garung_add_item_data($cart_item_data,$product_id)
    {
        global $woocommerce;
        // session_start();
        if (isset($_SESSION['wdm_user_custom_data'])) {
            $option = $_SESSION['wdm_user_custom_data'];
            $new_value = array('wdm_user_custom_data_value' => $option);
        }
        if(empty($option)){
            return $cart_item_data;
        }
        else
        {
            if(empty($cart_item_data)){
                return $new_value;
            }
            else{
                return array_merge($cart_item_data,$new_value);
            }
        }
        unset($_SESSION['wdm_user_custom_data']);
    }


    public function garung_get_cart_items_from_session($item, $values, $key)
    {
        if (array_key_exists('wdm_user_custom_data_value', $values)) {
            $item['wdm_user_custom_data_value'] = $values['wdm_user_custom_data_value'];
        }
        return $item;
    }

    public function garung_add_values_to_order_item_meta($item_id, $values)
    {
        global $woocommerce, $wpdb;
        $user_custom_values = $values['wdm_user_custom_data_value']['data'];
        $price = $user_custom_values['calc_price'];
        $title = $user_custom_values['title'];
        $color = $user_custom_values['color'];
        $title_chedobaohanh = $user_custom_values['title_chedobaohanh'];
        if (!empty($price)) {
            wc_add_order_item_meta($item_id, 'new_price', $price);
        }
        if (!empty($title)) {
            wc_add_order_item_meta($item_id, 'title_product', $title);
        }
        if (!empty($color)) {
            wc_add_order_item_meta($item_id, 'color_variable', $color);
        }
        if (!empty($title_chedobaohanh)) {
            wc_add_order_item_meta($item_id, 'title_chedobaohanh', $title_chedobaohanh);
        }
    }

    public function garung_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        $cart = $woocommerce->cart->get_cart();
        foreach( $cart as $key => $values)
        {
            if ( $values['wdm_user_custom_data_value'] == $cart_item_key ){
                unset( $woocommerce->cart->cart_contents[ $key ] );
            }
        }
    }

    public function garung_delete_item_cart_by_ajax() {
        $data['code'] = 404;
        if(empty($_POST['data_item_key'])) {
            return json_encode($data['code']);
        }
        $result = $this->woocommerce->cart->remove_cart_item($_POST['data_item_key'] );
        if($result) {
            $data['code'] = 200;
        }
        echo (json_encode($data['code']));
        exit();
    }

    public function garung_delete_all_cart() {
        $data['code'] = 404;
        if(($_POST['data_delete'] === 1) || ($_POST['data_delete'] === '1') || ($_POST['data_delete'] === true) ) {
            $this->woocommerce->cart->empty_cart();
            $data['code'] = 200;
        }
        echo (json_encode($data['code']));
        exit();
    }
    //============ demo - khong dung ===============
    public function garung_custom_create_order() {
        // echo "<pre>";
        // var_dump($_POST); die;
        global $wpdb, $woocommerce;

        // Give plugins the opportunity to create an order themselves
        if ( $order_id = apply_filters( 'woocommerce_create_order', null, $this ) ) {
            return $order_id;
        }

        try {
            wc_transaction_query( 'start' );

            $order_data = array(
                'status'        => apply_filters( 'woocommerce_default_order_status', 'processing' ),
                'customer_note' => isset( $_POST['order_comments'] ) ? $_POST['order_comments'] : '',
                'cart_hash'     => md5( json_encode( wc_clean( $woocommerce->cart->get_cart_for_session() ) ) . $woocommerce->cart->total ),
                'created_via'   => 'muangay',
            );

            $order_id = absint( WC()->session->order_awaiting_payment );
            /**
             * If there is an order pending payment, we can resume it here so
             * long as it has not changed. If the order has changed, i.e.
             * different items or cost, create a new order. We use a hash to
             * detect changes which is based on cart items + order total.
             */

            $order = wc_create_order( $order_data );
            if ( is_wp_error( $order ) ) {
                throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 520 ) );
            } elseif ( false === $order ) {
                throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 521 ) );
            } else {
                $order_id = $order->id;
                do_action( 'woocommerce_new_order', $order_id );
            }

            $subtotal = 0;
            $total = 0;
            foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
                if(!empty($values['wdm_user_custom_data_value'])){
                    $custom_values = $values['wdm_user_custom_data_value']['data'];
                    $price = $custom_values['calc_price'];

                    $subtotal += $price;
                }
            }
            $total = $subtotal + ($subtotal * 0.1);

            // Store the line items to the new/resumed order
            foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
                if(!empty($values['wdm_user_custom_data_value'])){
                    $custom_values = $values['wdm_user_custom_data_value']['data'];
                    $variable_id = $values['wdm_user_custom_data_value']['variable_id'];
                    $root_price = $custom_values['root_price'];
                    $price = $custom_values['calc_price'];
                    $title = $custom_values['title'];
                    $color = $custom_values['color'];
                    $title_chedobaohanh = $custom_values['title_chedobaohanh'];
                    $price_chedobaohanh = $custom_values['price_chedobaohanh'];

                    $variable_product = new WC_Product($variable_id);

                    $item_id = $order->add_product(
                        $values['data'],
                        $values['quantity'],
                        array(
                            'variation' => $variable_product,
                            'totals'    => array(
                                // 'subtotal'     => $root_price,
                                // 'total'        => $price * $values['quantity'],
                                'subtotal'     => $values['line_subtotal'],
                                'subtotal_tax' => $values['line_subtotal_tax'],
                                'total'        => $values['line_total'],
                                'tax'          => $values['line_tax'],
                                'tax_data'     => $values['line_tax_data'] // Since 2.2
                            )
                        )
                    );

                    if ( ! $item_id ) {
                        throw new Exception( sprintf( __( 'Lỗi %d: không tạo được hóa đơn. Làm ơn thử lại.', 'woocommerce' ), 525 ) );
                    }
                    wc_delete_order_item_meta($item_id, 'product_type');
                    wc_delete_order_item_meta($item_id, 'total_stock');
                     wc_add_order_item_meta($item_id, 'Loại mua', 'Mua ngay');
                    if(!empty($root_price)){
                        wc_add_order_item_meta($item_id, 'Giá gốc', wc_price($root_price));
                    }
                    if (!empty($values['quantity'])) {
                        wc_add_order_item_meta($item_id, 'Số lượng', $values['quantity']);
                    }
                    if (!empty($color)) {
                        wc_add_order_item_meta($item_id, 'Màu sản phẩm', $color);
                    }
                    if (!empty($title_chedobaohanh)) {
                        wc_add_order_item_meta($item_id, 'Chế độ bảo hành', $title_chedobaohanh);
                    }
                    if (!empty($price_chedobaohanh)) {
                        wc_add_order_item_meta($item_id, 'Giá chế độ bảo hành', wc_price($price_chedobaohanh));
                    }
                    wc_add_order_item_meta($item_id, 'Tổng giá', wc_price(($price) * $values['quantity']));
                } else {
                    return new WP_Error( 'checkout-error', $e->getMessage() );
                }
            }

            // Billing address
            $billing_address = array();
            if(!empty($_POST['billing_fullname'])) {
                $billing_address[ 'last_name' ] = $_POST['billing_fullname'];
            } else {
                $billing_address[ 'last_name' ] = '';
            }
            if(!empty($_POST['billing_email'])) {
                $billing_address[ 'email' ] = $_POST['billing_email'];
            } else {
                $billing_address[ 'email' ] = '';
            }
            if(!empty($_POST['billing_phone'])) {
                $billing_address[ 'phone' ] = $_POST['billing_phone'];
            } else {
                $billing_address[ 'phone' ] = '';
            }
            if(!empty($_POST['billing_address'])) {
                $address = $_POST['billing_address'];
            } else {
                $address = '';
            }
            if(!empty($_POST['billing_provice'])) {
                $provice = $_POST['billing_provice'];
            } else {
                $provice = '';
            }
            if(!empty($_POST['billing_district'])) {
                $district = $_POST['billing_district'];
            } else {
                $district = '';
            }

            var_dump($_POST['order_comments']);exit;

            $billing_address[ 'address_1' ] = $address.', '.$district.', '.$provice;
            $order->set_address( $billing_address, 'billing' );
            $order->set_payment_method( 'cod');
            wc_transaction_query( 'commit' );

        } catch ( Exception $e ) {
            // There was an error adding order data!
            wc_transaction_query( 'rollback' );
            return new WP_Error( 'checkout-error', $e->getMessage() );
        }

        return $order_id;
    }
    //==============================================
    /**
     * [create_order_for_tragop dung chung cho mua ngay va tra gop]
     * @return [int] [order_id]
     */
    public function create_order_for_tragop() {

        if(!isset($_POST)) {
            echo "$_POST"; exit;
        }
        global $wpdb, $woocommerce, $wp_session;
        // session_start();

        if ( $order_id = apply_filters( 'woocommerce_create_order', null, $this ) ) {
            return $order_id;
        }

        try {
            wc_transaction_query( 'start' );
            if(!isset($_POST['product_id']) || empty($_POST['product_id']) ) {
                // $_POST['product_id'] = '192';
            }

            $_product = new WC_Product_Variation( $_POST['variable_id'] );
            if(empty($_product)) {
                echo "$_POST"; exit;
            }
            $str_query_chedo = "SELECT ID, post_title FROM ".($wpdb->prefix)."posts WHERE post_status='publish' and post_type='chedobaohanh' and ID=".$_POST['chedobaohanh'];
            $chedobaohanh = $wpdb->get_row($str_query_chedo);
            // echo "<pre>";
            // var_dump($chedobaohanh);die;
            $chedobaohanh_price = '';
            $chedobaohanh_name = '';
            if(!empty($str_query_chedo)) {
                $chedobaohanh_name = $chedobaohanh->post_title;
                $chedobaohanh_price = get_field('gia_tien', $chedobaohanh->ID);
            }
            $name_product = $_product->post->post_title;
            $root_price = $_product->get_price();
            $name_color = $_POST['name_color'];
            $sum_price = $root_price + $chedobaohanh_price;
            if(isset($_POST['choose-pack-tragop'])) {
                $expire_tragop = $_POST['choose-pack-tragop'];
            } else {
                $expire_tragop = '';
            }
            if(isset($_POST['choose-tratruoc'])) {
                $precent_tratruoc = $_POST['choose-tratruoc'];
            } else {
                $precent_tratruoc = '';
            }
            
            if(!empty($_POST['order_comments'])) {
                $order_note = $_POST['order_comments'];
            } else {
                $order_note = '';
            }

            if(!empty($_POST['loaimua'])) {
                if(($_POST['loaimua'] == 'muangay')) {
                    $order_data = array(
                        'status'        => apply_filters( 'woocommerce_default_order_status', 'pending' ),
                        'customer_note' => $order_note,
                        'cart_hash'     => '',
                        'created_via'   => 'muangay',
                    );
                } else {
                    $order_data = array(
                        'status'        => apply_filters( 'woocommerce_default_order_status', 'pending' ),
                        'customer_note' => $order_note,
                        'cart_hash'     => '',
                        'created_via'   => 'tragop',
                    );
                }
            }

            $order = wc_create_order( $order_data );
            if ( is_wp_error( $order ) ) {
                throw new Exception( sprintf( __( 'Lỗi %d: Không tạo được hóa đơn. Xin vui lòng thử lại', 'woocommerce' ), 520 ) );
            } elseif ( false === $order ) {
                throw new Exception( sprintf( __( 'Lỗi %d: Không tạo được hóa đơn. Xin vui lòng thử lại', 'woocommerce' ), 521 ) );
            } else {
                $order_id = $order->id;
                do_action( 'woocommerce_new_order', $order_id );
            }

            $item_id = $order->add_product(
                $_product,
                1,
                array(
                    'variation' => '',
                    'totals'    => array(
                        'subtotal'     => $sum_price,
                        'subtotal_tax' => $root_price,
                        'total'        => $sum_price,
                        'tax'          => 0,
                        'tax_data'     => 0
                    )
                )
            );

            if ( ! $item_id ) {
                throw new Exception( sprintf( __( 'Lỗi %d: Không tạo được hóa đơn. Xin vui lòng thử lại.', 'woocommerce' ), 525 ) );
            }
            if(($_POST['loaimua'] == 'muangay')) {
                wc_add_order_item_meta($item_id, 'Loại mua', 'Mua ngay');
            } else {
                wc_add_order_item_meta($item_id, 'Loại mua', 'Trả góp');
            }

            if (!empty($name_product)) {
                wc_add_order_item_meta($item_id, 'Tên sản phẩm', $name_product);
            }
            if (!empty($root_price)) {
                wc_add_order_item_meta($item_id, 'Giá gốc', wc_price($root_price));
            }
             wc_add_order_item_meta($item_id, 'Số lượng', '1');
            if (!empty($name_color)) {
                wc_add_order_item_meta($item_id, 'Màu sắc', $name_color);
            }
            if (!empty($chedobaohanh_name)) {
                wc_add_order_item_meta($item_id, 'Chế độ bảo hành', $chedobaohanh_name);
            }
            if (!empty($chedobaohanh_price)) {
                wc_add_order_item_meta($item_id, 'Giá bảo hành', wc_price($chedobaohanh_price));
            }
            if(!empty($precent_tratruoc)){
                wc_add_order_item_meta($item_id, '% trả trước', $precent_tratruoc . '%');
            }
            if(!empty($expire_tragop)){
                wc_add_order_item_meta($item_id, 'Hạn trả góp', $expire_tragop . ' tháng');
            }
            if(!empty($precent_tratruoc) && !empty($expire_tragop)){
                wc_add_order_item_meta($item_id, 'Tiền trả trước', wc_price(($sum_price * $precent_tratruoc)/100));
                $tien_tra_hangthang =  ($sum_price - (($sum_price * $precent_tratruoc)/100))/$expire_tragop;
                wc_add_order_item_meta($item_id, 'Trả hàng tháng', wc_price(abs($tien_tra_hangthang)).'/tháng');
            }
            wc_add_order_item_meta($item_id, 'Tổng trả', wc_price($sum_price));

            // Billing address
            $billing_address = array();
            if(!empty($_POST['billing_fullname'])) {
                $billing_address[ 'fullname' ] = $_POST['billing_fullname'];
            } else {
                $billing_address[ 'fullname' ] = '';
            }
            if(!empty($_POST['billing_email'])) {
                $billing_address[ 'email' ] = $_POST['billing_email'];
            } else {
                $billing_address[ 'email' ] = '';
            }
            if(!empty($_POST['billing_phone'])) {
                $billing_address[ 'phone' ] = $_POST['billing_phone'];
            } else {
                $billing_address[ 'phone' ] = '';
            }
            if(!empty($_POST['billing_address'])) {
                $address = $_POST['billing_address'];
            } else {
                $address = '';
            }
            if(!empty($_POST['billing_provice'])) {
                $provice = $_POST['billing_provice'];
            } else {
                $provice = '';
            }
            if(!empty($_POST['billing_district'])) {
                $district = $_POST['billing_district'];
            } else {
                $district = '';
            }
            $billing_address[ 'address_1' ] = $address.', '.$district.', '.$provice;

            $order->set_address( $billing_address, 'billing' );
            $order->set_address( $shipping_address, 'shipping' );

            $order->customer_message = $_POST['order_comments'];
            $order->customer_note = $_POST['order_comments'];
            $order->order_comments = $_POST['order_comments'];

            wc_transaction_query( 'commit' );

        } catch ( Exception $e ) {
            wc_transaction_query( 'rollback' );
            return new WP_Error( 'checkout-error', $e->getMessage() );
        }
        // echo "<pre>";
        // var_dump($woocommerce->get('orders/'.$order_id)); die;;
        if(!empty($order_id)) {
            if(isset($_SESSION['order_id'])) {
                unset($_SESSION['order_id']);
            }
            $_SESSION['order_id'] = $order_id;
            wp_redirect(site_url('/checkout-tra-gop/'));
            // echo view('../checkout/success-tragop');
        } else {
            return new WP_Error( 'checkout-error', $e->getMessage() );
        }
    }
}

new customize_woocomerce();
