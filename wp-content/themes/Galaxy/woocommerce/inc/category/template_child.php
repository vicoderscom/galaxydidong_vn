<?php
if ($hasAdv == true) {
    if ($key != 5) {
    ?>
        <a href="<?php echo $item->link; ?>">
            <div class="col-md-2 col-sm-6 col-xs-12 border-product item_product motsanpham">
                <div class="product">
                    <div style="background-image: url(<?php echo $item->image; ?>);" class="img_item">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/tranparent.png" alt="">
                    </div>
                    <span class="name-product text-center">
                        <h5><?php echo $item->post_title; ?></h5>
                    </span>
                    <span class="product-price">
                        <h3><?php echo $item->price; ?></h3>
                    </span>
                    <?php  
                    if($term_slug !== 'phukien'):
                    ?>
                    <div class="tragop">
                        Trả góp từ: <span class="data-change-price"><?php echo wc_price(($item->info_product->get_price() - ($item->info_product->get_price() *0.3))/6); ?></span>
                    </div>
                    <?php 
                    endif;
                    ?>
                    </span>
                    </span>
                </div>
                <?php  
                if($term_slug != 'phukien'):
                ?>
                <div class="info_hover" style="text-align: left;">
                	<p>CPU: <?php echo $item->cpu; ?></p>
                	<p>Ram: <?php echo $item->ram; ?></p>
                	<p>Bộ nhớ trong: <?php echo $item->rom; ?></p>
                	<p>Màn hình <?php echo $item->screen; ?></p>
                	<p>Camera <?php echo $item->camera; ?></p>
                	<p>OS: <?php echo $item->oper_system; ?></p>
                	<p>Pin: <?php echo $item->pin; ?></p>
                </div>
                <?php  endif; ?>
                <div class="row two-button">
                    <div class="<?php echo ($term_slug !== 'phukien') ? 'col-md-6 col-sm-6 col-xs-6 pull-left left' : 'col-md-12 col-sm-12 col-xs-12 text-center' ?>">
                        <a href="<?php  the_permalink(); ?>"><button style="border: unset;<?php echo ($term_slug !== 'phukien') ? '' : 'float: none !important;' ?>" class="btn btn-primary btn-muangay">MUA</button>
                        </a>
                    </div>
                    <?php  
                    if($term_slug != 'phukien'):
                    ?>
                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right right">
                        <form action="<?php echo site_url('tra-gop'); ?>" method="post">
                            <input type="hidden" name="product-id" value="<?php echo $item->ID; ?>">
                            <button class="btn btn-success btn-tragop" type="submit">
                                TRẢ GÓP
                            </button>
                        </form>
                    </div>
                    <?php  endif; ?>
                </div>
            </div>
        </a>

    <?php

    } else {
    ?>
        <div class="col-md-4 quangcao item_product_qq item_product_qq_img item_product">
            <div class="img">
                <a href='<?php echo $item->link; ?>'>
                    <img class="img-responsive product-didong2 cover_img" style="background-image:url('<?php echo $item->image; ?>');" src="<?php echo get_template_directory_uri(); ?>/assets/images/trans_cat.png" alt="galaxy">
                </a>
            </div>
            <?php  
            if($term_slug !== 'phukien'):
            ?>
            <div class="info_hover" style="text-align: left;">
                <p>CPU: <?php echo $item->cpu; ?></p>
                <p>Ram: <?php echo $item->ram; ?></p>
                <p>Bộ nhớ trong: <?php echo $item->rom; ?></p>
                <p>Màn hình <?php echo $item->screen ?></p>
                <p>Camera <?php echo $item->camera; ?></p>
                <p>OS: <?php echo $item->oper_system; ?></p>
                <p>Pin: <?php echo $item->pin; ?></p>
            </div>
            <?php  endif; ?>
            <div class="row row-sanpham advertisement_cat_row" style="position: absolute;bottom: 0;width: 100%;">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="tieude-sanpham advertisement_cat">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </div>

                    <div class="gia-sanpham" data-price="<?php echo $item->info_product->get_price(); ?>">
                        <?php echo $item->price; ?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 buy_now_product">
                    <div class="row two-button">
                        <div class="<?php echo ($term_slug !== 'phukien') ? 'col-md-6 col-sm-6 col-xs-6 pull-left left' : 'col-md-12 col-sm-12 col-xs-12 text-center' ?>">
                            <a href="<?php the_permalink(); ?>"><button style="border: unset;<?php echo ($term_slug !== 'phukien') ? '' : 'float: none !important;' ?>" class="btn btn-primary btn-muangay">MUA NGAY</button>
                            </a>
                        </div>
                        <?php  
                        if($term_slug !== 'phukien'):
                        ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                            <form action="<?php echo site_url('tra-gop'); ?>" method="post">
                                <input type="hidden" name="product-id" value="<?php echo $item->ID; ?>">

                                <button class="btn btn-success btn-tragop" type="submit">
                                    TRẢ GÓP
                                </button>
                            </form>
                        </div>
                        <?php  endif; ?>
                    </div>
                </div>
            </div>
        </div>

    <?php
    }
} else {
    ?>
    <a href="<?php echo $item->link; ?>">
        <div class="col-md-2 col-sm-6 col-xs-12 border-product item_product motsanpham">
            <div class="product">
                <div style="background-image: url(<?php echo $item->image; ?>);" class="img_item">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/tranparent.png" alt="">
                </div>
                <span class="name-product text-center">
                    <h5><?php echo $item->post_title; ?></h5>
                </span>
                <span class="product-price">
                    <h3><?php echo $item->price; ?></h3>
                </span>
                <?php  
                if($term_slug !== 'phukien'):
                ?>
                <div class="tragop">
                    Trả góp từ: <span class="data-change-price"><?php echo wc_price(($item->info_product->get_price() - ($item->info_product->get_price() *0.3))/6); ?></span>
                </div>
                <?php 
                endif;
                ?>  
                </span>
                </span>
            </div>
            <?php  
            if($term_slug !== 'phukien'):
            ?>
            <div class="info_hover" style="text-align: left;">
                <p>CPU: <?php echo $item->cpu; ?></p>
                <p>Ram: <?php echo $item->ram; ?></p>
                <p>Bộ nhớ trong: <?php echo $item->rom; ?></p>
                <p>Màn hình <?php echo $item->screen; ?></p>
                <p>Camera <?php echo $item->camera; ?></p>
                <p>OS: <?php echo $item->oper_system; ?></p>
                <p>Pin: <?php echo $item->pin; ?></p>
            </div>
            <?php  endif; ?>
            <div class="row two-button">
                <div class="<?php echo ($term_slug !== 'phukien') ? 'col-md-6 col-sm-6 col-xs-6 pull-left left' : 'col-md-12 col-sm-12 col-xs-12 text-center' ?>">
                    <a href="<?php  the_permalink(); ?>"><button style="border: unset;<?php echo ($term_slug !== 'phukien') ? '' : 'float: none !important;' ?>" class="btn btn-primary btn-muangay">XEM NGAY</button>
                    </a>
                </div>
                <?php  
                if($term_slug !== 'phukien'):
                ?>
                <div class="col-md-6 col-sm-6 col-xs-6 pull-right right">
                    <form action="<?php echo site_url('tra-gop'); ?>" method="post">
                        <input type="hidden" name="product-id" value="<?php echo $item->ID; ?>">
                        <button class="btn btn-success btn-tragop" type="submit">
                            TRẢ GÓP
                        </button>
                    </form>
                </div>
                <?php  endif; ?>
            </div>
        </div>
    </a>
    <?php
}
