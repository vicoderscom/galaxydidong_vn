<?php
global $product;
$cat_page = get_queried_object();
$cat_page_id = $cat_page->term_id;
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$url_page = '//' . $_SERVER['HTTP_HOST'] . $uri_parts[0];

$price_1 = get_option('price_value_1');
$price_2 = get_option('price_value_min_2');
$price_3 = get_option('price_value_max_2');
$price_4 = get_option('price_value_min_3');
$price_5 = get_option('price_value_max_3');
$price_6 = get_option('price_value_4');
$price_7 = get_option('price_value_min_5');
$price_8 = get_option('price_value_max_5');
$price_9 = get_option('price_value_min_6');
$price_10 = get_option('price_value_max_6');
$price_11 = get_option('price_value_min_7');
$price_12 = get_option('price_value_max_7');
$price_13 = get_option('price_value_min_8');
$price_14 = get_option('price_value_max_8');
$price_15 = get_option('price_value_9');
$price_16 = get_option('price_value_10');

// $price_1 = ;
if (!empty($_GET['min'])) {
	$min_price = (int) ($_GET['min']);
} else {
	$min_price = 0;
}
if (!empty($_GET['max'])) {
	$max_price = (int) ($_GET['max']);
} else {
	$max_price = 0;
}

if (isset($_GET['price_min'])) {
	$order_price = 'asc';
} elseif (isset($_GET['price_max'])) {
	$order_price = 'desc';
} else {
	$order_price = 'asc';
}
if (isset($_GET['status'])) {
	$status = $_GET['status'];
}
// echo $price_value_1;