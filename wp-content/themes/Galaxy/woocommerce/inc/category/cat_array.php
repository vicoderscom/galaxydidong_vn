<?php
$term = get_queried_object();

$advProduct = get_field('adv_product', $term);
$term_slug = $term->slug;

$paged = (get_query_var('paged')) ? (get_query_var('paged')) : 1;

$product_cat_sort = array(
	'post_type' => 'product',
	'post_status' => 'publish',
	'ignore_sticky_posts' => 1,
	'posts_per_page' => '25',

	'meta_query' => array(
		array(
			'key' => '_visibility',
			'value' => array('catalog', 'visible'),
			'compare' => 'IN',
		),
	),
	'tax_query' => array(
		array(
			'taxonomy' => 'product_cat',
			'field' => 'term_id', //This is optional, as it defaults to 'term_id'
			'terms' => $cat_page_id,
			'operator' => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
		),
	),
	'paged' => $paged,
	'orderby' => 'meta_value_num',
	'meta_key' => '_price',
	'order' => $order_price,
);
$product_cat_sort_array = array(
	'orderby' => array(
		'date' => 'desc',
	),
);
if (!isset($order_price) && !isset($min_price) && !isset($status) && !isset($max_price)) {
	unset($product_cat_sort['orderby']);
	unset($product_cat_sort['order']);
	unset($product_cat_sort['meta_key']);
	$product_cat_sort = array_merge($product_cat_sort, $product_cat_sort_array);
}
$products = new WP_Query($product_cat_sort);
$items = $products->posts;
foreach ($items as $key => $item) {

	if(has_post_thumbnail($item->ID)) {
	   $item->image = wp_get_attachment_url(get_post_thumbnail_id($item->ID));
	} else {
	   $item->image = get_template_directory_uri().'/assets/images/no_image_2.png';
	}
	$item->link = get_permalink($item->ID);
	if(isset(get_post_meta($item->ID)['san_pham_co_anh_lon_khong'][0])) {
	    $item->custom_field = get_post_meta($item->ID)['san_pham_co_anh_lon_khong'][0];
	} else {
		$item->custom_field = '';
	}
	if(isset(get_post_meta($item->ID)['tinh_trang'][0])) {
	    $item->status = get_post_meta($item->ID)['tinh_trang'][0];
	} else {
		$item->status = '';
	}
	$item->info_product = wc_get_product($item->ID);
	$item->price_no_format = str_replace('.', '', $item->info_product->get_price());
	$item->regular_price = wc_price($item->info_product->get_regular_price(), array());
	$item->sale_price = $item->info_product->get_sale_price();
	$item->price = wc_price($item->info_product->get_price(), array());
	$item->sale = (int) $item->regular_price - (int) $item->sale_price;
	$item->cpu = get_post_meta($item->ID)['acf_cpu'][0];
	$item->ram = get_post_meta($item->ID)['acf_ram'][0];
	$item->rom = get_post_meta($item->ID)['acf_bonhotrong'][0];
	$item->screen = get_post_meta($item->ID)['acf_manhinh'][0];
	$item->camera = get_post_meta($item->ID)['acf_camera'][0];
	$item->oper_system = get_post_meta($item->ID)['acf_hedieuhanh'][0];
	$item->pin = get_post_meta($item->ID)['acf_pin'][0];

	// remove adv product out cat array
	if ($advProduct != null) {
		if ($item->ID == $advProduct->ID) {
			$advProductFullData = $item;
			unset($items[$key]);
		}
	}
}

//var_dump($advProduct);
$hasAdv = false;

if (isset($advProductFullData)) {
	array_splice($items, 5, 0, [$advProductFullData]);
	$hasAdv = true;
}

//var_dump($items);
$array_cat = [
	'type'                     => 'product',
	'parent'                   => $cat_page_id,
	'taxonomy'                 => 'category',
];
$cat_terms = get_terms('product_cat', $array_cat);
foreach($cat_terms as $cat_term) {
	$cat_term->link = get_category_link($cat_term->term_id);
}
