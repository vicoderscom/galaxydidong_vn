=== YITH WooCommerce Product Add-Ons Premium ===

Contributors: yithemes
Tags: woocommerce, product, option, form, attribute, variation, radio, checkbox, label, upload, image
Requires at least: 4.4
Tested up to: 4.6.1
Stable tag: 1.2.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.2.0.3 =

* Fixed: Total preview was not updated right after variations was changed.
* Fixed: First element with the add-ons "select" was not stored in the cart.

= 1.2.0.2 =

* Added: Hide price feature with YITH WooCommerce Catalog Mode Premium and YITH WooCommerce Requeste a Quote Premium.
* Fixed: Labels and descriptions of the Add-Ons were not translated on the customer email even if translation was complete on WPML String Translations.

= 1.2.0.1 =

* Fixed: Add-on with dependence doesn' t appear even if the correct variation was selected.
* Fixed: Prevent notice in the back-end when a new add-on was inserted.


= 1.2.0 =

= Add-Ons =

* Added: Possibility to hide add-ons until a specified option or variation is selected.
* Added: Integration with YITH WooCommerce Role Based Price.
* Added: Flatsome quick view compatibility.
* Added: Exclude products field on group
* Fixed: Click doesn' t fire on radio button label.
* Fixed: Error was printed when a customer receives YITH WooCommerce Request a quote email.
* Fixed: Add-ons name and value was not translated by WPML on the Cart

= Variations =

* Added: Change product image on hover (only for one attirbute).
* Added: Option to show custom attributes style also on "Additional Information" Tab.
* Added: Compatibility with WooCommerce Products Filter.
* Added: Compatibility with YITH Composite Products For WooCommerce.
* Added: Compatibility with WooCommerce Quick View by WooThemes.
* Fixed: Reset attribute type on plugin deactivation.
* Fixed: Description and default variations on archive pages.
* Updated: Language files.
* Updated: Core plugin.

= 1.1.4 = Released on Jul 08, 2016

* Updated:  Language files.
* Fixed: Wrong total price preview when variation is changed
* Fixed: Default variation on single product pages for products with only one attribute
* Fixed: Issue when there were two labels in two different group

= 1.1.3 =

* Added:  WooCommerce 2.6 support.
* Added:  Option "Max Items Selected" for checkboxes add ons

= 1.1.2 =

* Updated:  Language files.
* Fixed:    jQuery event not triggered with "The Edge / Internet Explorer" browser
* Fixed:    Product Add-On Group is not saved because of mysql error

= 1.1.1 =

* Fixed: error on add to cart when add-on is not "sold individually"

= 1.1.0 =

* Added: Support to WordPress 4.5.2.
* Added: Support to WooCommerce 2.6 Beta2.

= Add-Ons =

* Added: "Sold individually" add-ons option that allow user to sell an add-on lonely(* the price will not increases by cart quantity)
* Added: "Upoad File size" option on settings that allow the administrator to set max uploaded file size
* Added: "Vendor" option on group that allow administrator to change the vendor previously store
* Added: Option "Show product price on 'cart page'" that allow you to show the product base price on the cart item
* Fixed: minor bugs

= Variations =

* Added: Compatibility with YITH WooCommerce Added to Cart Popup.
* Added: Set dual color such as blue-white (half box blue and half box white).
* Added: Show a preview of the attribute image in the tooltip (available only for image attributes).
* Fixed: Variations now work with Owl Carousel 2 when infinite loop option is set.
* Fixed: Clicking on selected attribute before selecting another one is no longer necessary.
* Updated: Language files.
* Updated: Core plugin.

= 1.0.9 =

*Fixed: prevent localize domain issue

= 1.0.8 =

*Added: support to YITH WooCommerce Request a Quote - 1.4.7 version

= 1.0.7 =

*Updated: Text Domain
*Fixed: minor bugs

= 1.0.6 =

*Fixed: Prevent notice on products loop

= 1.0.5 =

*Added: WordPress 4.5 support

= 1.0.4 =

*Fixed: Request a quote button not working in the products loop
*Fixed: Removed unuseless query execution

= 1.0.3 =

* Added: WPML support
* Fixed: Options total price was not correct when user change quantity on single product page

= 1.0.2 =

* Fixed: Options are not saved when a quote was inserted inside a label

= 1.0.1 =

* Fixed: Price total doesn' t change after option is selected on quick view

= 1.0.0 =

Initial Release
