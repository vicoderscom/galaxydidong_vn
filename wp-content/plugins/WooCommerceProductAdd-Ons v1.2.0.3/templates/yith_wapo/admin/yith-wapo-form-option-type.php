<?php
/**
 * Admin Type Form
 *
 * @author  Yithemes
 * @package YITH WooCommerce Product Add-Ons
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or exit;

$is_edit = isset( $type );
$act = 'new';
$priority = 0;
$field_type = '';
$field_image_url = YITH_WAPO_URL . '/assets/img/placeholder.png';
$field_image = '';
$field_id_img_class = 'form-add';
$field_label = '';
$field_description = '';
$field_required = false;
$field_qty_individually = false;
$field_max_item_selected = 0;
$field_description = '';
$field_priority = '';

$dependencies_query = YITH_WAPO_Admin::getDependeciesQuery( $wpdb, $group , $type, $is_edit );

if( $is_edit ) {
    $act = 'update';
    $field_priority = $type->priority;
    $field_type = $type->type;
    if(  $type->image ) {
        $field_image_url = $field_image = $type->image;
    }
    $field_id_img_class = $type->id;
    $field_label = $type->label;
    $field_required = $type->required;
    $field_description = $type->description;
    $field_qty_individually = $type->sold_individually;
    $field_max_item_selected = $type->max_item_selected;
}
?>

<form action="edit.php?post_type=product&page=yith_wapo_group" method="post" class="<?php echo $field_type; ?>">

    <?php if( $is_edit ) : ?>

        <input type="hidden" name="id" value="<?php echo $type->id; ?>">

    <?php endif; ?>

    <input type="hidden" name="act" value="<?php echo $act; ?>">
    <input type="hidden" name="class" value="YITH_WAPO_Type">
    <input type="hidden" name="group_id" value="<?php echo $group->id; ?>">
    <input type="hidden" name="priority" value="<?php echo $field_priority; ?>">

    <div class="form-left">
        <div class="form-row">
            <div class="type">
                <label for="label"><?php echo __( 'Add-on', 'yith-woocommerce-product-add-ons' ); ?></label>
                <select name="type">
                    <option value="checkbox" <?php selected( $field_type , 'checkbox' ); ?>><?php _e( 'Checkbox' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="color" <?php selected( $field_type , 'color'); ?>><?php _e( 'Color' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="date" <?php selected( $field_type , 'date'); ?>><?php _e( 'Date' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="labels" <?php selected( $field_type , 'labels'); ?>><?php _e( 'Labels' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="number" <?php selected( $field_type , 'number'); ?>><?php _e( 'Number' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="select" <?php selected( $field_type , 'select'); ?>><?php _e( 'Select' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="radio" <?php selected( $field_type , 'radio'); ?>><?php _e( 'Radio Button' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="text" <?php selected( $field_type , 'text'); ?>><?php _e( 'Text' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="textarea" <?php selected( $field_type , 'textarea'); ?>><?php _e( 'Textarea' , 'yith-woocommerce-product-add-ons' )  ?></option>
                    <option value="file" <?php selected( $field_type , 'file'); ?>><?php _e( 'Upload' , 'yith-woocommerce-product-add-ons' )  ?></option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="image">
                <label for="image"><?php echo __( 'Image', 'yith-woocommerce-product-add-ons' ); ?></label>

                <input class="image" type="hidden" name="image" size="60" value="<?php echo $field_image; ?>">
                <img class="thumb image image-upload" src="<?php echo $field_image_url; ?>" height="100" />
                <span class="dashicons dashicons-no remove"></span>

                <script>
                    jQuery(document).ready( function($) {
                        $('#type-<?php echo $field_id_img_class; ?> .image-upload').click( function(e) {
                            e.preventDefault();
                            var parent = $(this).parent();
                            var custom_uploader = wp.media({
                                    title: 'Custom Image',
                                    button: { text: 'Upload Image' },
                                    multiple: false  // Set this to true to allow multiple files to be selected
                                })
                                .on('select', function() {
                                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                                    $('.image', parent).attr('src', attachment.url );
                                    $('.image', parent).val(attachment.url );

                                })
                                .open();
                        });
                        $('#type-<?php echo $field_id_img_class; ?> .remove').click( function(){
                            var parent = $(this).parent();
                            $('.image', parent).attr('src', '<?php echo YITH_WAPO_URL; ?>/assets/img/placeholder.png' );
                            $('.image', parent).val('');
                        });

                        var tiptip_args = {
                            'attribute': 'data-tip',
                            'fadeIn': 50,
                            'fadeOut': 50,
                            'delay': 200
                        };
                        $( '.woocommerce-help-tip' ).tipTip( tiptip_args );
                        
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="form-right">
        <div class="form-row">
            <div class="label">
                <label for="label"><?php _e( 'Title', 'yith-woocommerce-product-add-ons' ); ?></label>
                <input name="label" type="text" value="<?php echo $field_label; ?>" class="regular-text">
            </div>
            <div class="depend">

                <label for="depend"><?php _e( 'Add-On Requirements', 'yith-woocommerce-product-add-ons' ); //@since 1.1.0 ?><span class="woocommerce-help-tip" data-tip="<?php _e( 'Show this add-on to users only if they have first selected the following options.', 'yith-woocommerce-product-add-ons' ); //@since 1.1.0 ?>"></span></label>

                <select name="depend[]" class="depend-select2" multiple="multiple" placeholder="<?php echo __( 'Choose required add-ons', 'yith-woocommerce-product-add-ons' ); ?>..."><?php

                    $dependencies = $wpdb->get_results( $dependencies_query );

                    foreach ( $dependencies as $key => $item ) {

                        $depend_array = explode( ',', $type->depend );

                        echo '<option value="' . $item->id . '"' . ( in_array( $item->id, $depend_array ) ? 'selected="selected"' : '' ) . '>' . esc_html( $item->label ).' [ '. __( 'Any Value' , 'yith-woocommerce-product-add-ons' ) . ' ]</option>';

                        $options_values = maybe_unserialize( $item->options );

                        if( isset( $options_values['label'] ) ) {

                            foreach ( $options_values['label'] as $option_key => $option_value ) {
                                $attribute_value = 'option_' . $item->id . '_'.$option_key;
                                echo '<option value="'.esc_attr( $attribute_value ).'" '.( in_array( $attribute_value, $depend_array ) ? 'selected="selected"' : '' ).'>' . esc_html( $item->label ).' [ '.$option_value . ' ]</option>';
                            }

                        }

                    }

                    ?>
                </select>

            </div>
            <div class="variations">

                <?php

                if( isset( $type ) ) {
                    $depend_variations_array = explode( ',', $type->depend_variations );
                } else {
                    $depend_variations_array = array();
                }

                ?>

                <label for="variations"><?php _e( 'Variations Requirements', 'yith-woocommerce-product-add-ons' ); //@since 1.1.0 ?><span class="woocommerce-help-tip" data-tip="<?php _e( 'Show this add-on to users only if they have first selected one of the following variations.', 'yith-woocommerce-product-add-ons' ); //@since 1.1.0 ?>"></span></label>

                <select name="depend_variations[]" class="depend-select2" multiple="multiple" placeholder="<?php echo __( 'Choose required variations', 'yith-woocommerce-product-add-ons' ); ?>...">
                    <?php YITH_WAPO_Admin::echo_product_chosen_list( $group->products_id , $group->categories_id , $depend_variations_array ); ?>
                </select>

            </div>
            <div class="required">
                <label for="required"><?php echo __( 'Required', 'yith-woocommerce-product-add-ons' ); ?><span class="woocommerce-help-tip" data-tip="<?php _e( 'Check this option if you want that the add-on have to be selected', 'yith-woocommerce-product-add-ons' ); //@since 1.1.3 ?>"></span></label>
                <input type="checkbox" name="required" value="1" <?php echo $field_required ? 'checked="checked"' : ''; ?>>
            </div>
            <div class="sold_individually">
                <label for="required"><?php echo __( 'Sold individually', 'yith-woocommerce-product-add-ons' ); //@since 1.1.0 ?><span
                        class="woocommerce-help-tip" data-tip="<?php _e( 'Check this box if you want that the selected add-ons are not increased as
                        the product quantity changes.', 'yith-woocommerce-product-add-ons' ); //@since 1.1.0 ?>"></span></label>
                <input type="checkbox" name="sold_individually" value="1" <?php echo $field_qty_individually ? 'checked="checked"' : ''; ?>>
            </div>
            <div class="max_item_selected">
                <label for="max_item_selected"><?php echo __( 'Limit selectable elements', 'yith-woocommerce-product-add-ons' ); //@since 1.1.3 ?><span class="woocommerce-help-tip" data-tip="<?php _e( 'Set the maximum number of elements that users can select for this add-on, 0 means no limits (works only with checkboxes)', 'yith-woocommerce-product-add-ons' ); //@since 1.1.3 ?>"></span></label>
                <input name="max_item_selected" type="number" value="<?php echo $field_max_item_selected; ?>" class="regular-text" min="0">
            </div>
        </div>
        <div class="form-row">
            <div class="description">
                <label for="description"><?php echo __( 'Description', 'yith-woocommerce-product-add-ons' ); ?></label>
                <textarea name="description" id="description" rows="3" style="width: 100%;"><?php echo $field_description; ?></textarea>
            </div>
        </div>
        
        <?php if( $is_edit ) : ?>

            <div class="form-row">
                <div class="options">
                    <table class="wp-list-table widefat fixed">
                        <tr>
                            <th class="option-image"><?php echo __( 'Image', 'yith-woocommerce-product-add-ons' );?></th>
                            <th class="option-label"><?php echo __( 'Option Label', 'yith-woocommerce-product-add-ons' );?></th>
                            <th class="option-type"><?php echo __( 'Type', 'yith-woocommerce-product-add-ons' );?></th>
                            <th class="option-price"><?php echo __( 'Price', 'yith-woocommerce-product-add-ons' );?></th>
                            <th class="option-min"><?php echo __( 'Min', 'yith-woocommerce-product-add-ons' );?></th>
                            <th class="option-max"><?php echo __( 'Max', 'yith-woocommerce-product-add-ons' );?></th>
                            <!--<th class="option-description"><?php echo __( 'Description', 'yith-woocommerce-product-add-ons' );?></th>-->
                            <th class="option-delete"></th>
                        </tr>
                        <?php
                        $i = 0;
                        $array_options = maybe_unserialize( $type->options );
                        if ( ! isset($array_options['description'] ) ) { $array_options['description'] = ''; }
                        if ( isset( $array_options['label'] ) && is_array( $array_options['label'] ) ) {
                            $array_default = isset( $array_options['default'] ) ? $array_options['default'] : array();
                            $array_required = isset( $array_options['required'] ) ? $array_options['required'] : array();
                            foreach ( $array_options['label'] as $key => $value ) : ?>
                                <tr>
                                    <td>
                                        <div id="option-image-<?php echo $i; ?>" class="option-image">
                                            <div class="image">
                                                <?php

                                                $image_url = YITH_WAPO_URL . '/assets/img/placeholder.png';

                                                if ( isset( $array_options['image'] ) && isset( $array_options['image'][$i] ) && $array_options['image'][$i] ) { $image_url = $array_options['image'][$i]; }

                                                ?>

                                                <input class="opt-image" type="hidden" name="options[image][]" size="60" value="<?php echo isset( $array_options['image'] ) ? $array_options['image'][$i] : ''; ?>">
                                                <img class="thumb opt-image opt-image-upload" src="<?php echo $image_url; ?>" height="100" />
                                                <!--<a href="#" class="button opt-image-upload"><?php echo __( 'Upload', 'cdz' ); ?></a>-->
                                                <span class="dashicons dashicons-no opt-remove"></span>

                                                <script>
                                                    jQuery(document).ready( function($) {
                                                        $('#type-<?php echo $type->id; ?> #option-image-<?php echo $i; ?> .opt-image-upload').click( function(e) {
                                                            e.preventDefault();
                                                            var parent = $(this).parent();
                                                            var custom_uploader = wp.media({
                                                                    title: 'Custom Image',
                                                                    button: { text: 'Upload Image' },
                                                                    multiple: false  // Set this to true to allow multiple files to be selected
                                                                })
                                                                .on('select', function() {
                                                                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                                                                    $('.opt-image', parent).attr('src', attachment.url );
                                                                    $('.opt-image', parent).val(attachment.url );

                                                                })
                                                                .open();
                                                        });
                                                        $('#type-<?php echo $type->id; ?> #option-image-<?php echo $i; ?> .opt-remove').click( function(){
                                                            var parent = $(this).parent();
                                                            $('.opt-image', parent).attr('src', '<?php echo YITH_WAPO_URL; ?>/assets/img/placeholder.png' );
                                                            $('.opt-image', parent).val('');
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="6">
                                        <div class="option-image"></div>
                                        <div class="option-label"><input type="text" name="options[label][]" value="<?php echo stripslashes( $array_options['label'][$i] ); ?>" placeholder="Label" /></div>
                                        <div class="option-type">
                                            <select name="options[type][]">
                                                <option value="fixed" <?php echo isset( $array_options['type'][$i] ) && $array_options['type'][$i] == 'fixed' ? 'selected="selected"' : ''; ?>><?php echo __( 'Fixed amount', 'yith-woocommerce-product-add-ons' ); ?></option>
                                                <option value="percentage" <?php echo isset( $array_options['type'][$i] ) && $array_options['type'][$i] == 'percentage' ? 'selected="selected"' : ''; ?>><?php echo __( '% markup', 'yith-woocommerce-product-add-ons' ); ?></option>
                                            </select>
                                            <!--<input type="text" name="options[type][]" value="<?php echo $array_options['type'][$i]; ?>" placeholder="0" />-->
                                        </div>
                                        <div class="option-price"><input type="text" name="options[price][]" value="<?php echo $array_options['price'][$i]; ?>" placeholder="0" /></div>
                                        <div class="option-min"><input type="text" name="options[min][]" value="<?php echo isset( $array_options['min'][$i] ) ? $array_options['min'][$i] : ''; ?>" placeholder="0" /></div>
                                        <div class="option-max"><input type="text" name="options[max][]" value="<?php echo isset( $array_options['max'][$i] ) ? $array_options['max'][$i] : ''; ?>" placeholder="0" /></div>
                                        <div class="option-delete"><a class="button remove-row"><?php echo __( 'Delete', 'yith-woocommerce-product-add-ons' ); ?></a></div>
                                        <div class="option-description" colspan="6"><input type="text" name="options[description][]" value="<?php echo stripslashes( $array_options['description'][$i] ); ?>" placeholder="Description" /></div>
                                        <div class="option-default">
                                            <input type="checkbox" name="options[default][]" value="<?php echo $i; ?>"
                                                <?php foreach ( $array_default as $key_def => $value_def ) { echo $i == $value_def ? 'checked="checked"' : ''; } ?> />
                                            <?php echo __( 'Checked', 'yith-woocommerce-product-add-ons' );?>
                                        </div>
                                        <div class="option-required">
                                            <input type="checkbox" name="options[required][]" value="<?php echo $i; ?>"
                                                <?php foreach ( $array_required as $key_def => $value_def ) { echo $i == $value_def ? 'checked="checked"' : ''; } ?> />
                                            <?php echo __( 'Required', 'yith-woocommerce-product-add-ons' );?>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++;
                            endforeach;
                        }
                        ?>
                        <tr>
                            <td>
                                <div id="option-image-new" class="option-image">
                                    <p class="save-first"><?php echo __( 'Save to set image!', 'yith-woocommerce-product-add-ons' ); ?></p>
                                </div>
                            </td>
                            <td colspan="6">
                                <div class="option-label"><input type="text" name="options[label][]" value="" placeholder="Label" /></div>
                                <div class="option-type">
                                    <select name="options[type][]">
                                        <option value="fixed"><?php echo __( 'Fixed amount', 'yith-woocommerce-product-add-ons' ); ?></option>
                                        <option value="percentage"><?php echo __( '% markup', 'yith-woocommerce-product-add-ons' ); ?></option>
                                    </select>
                                </div>
                                <div class="option-price"><input type="text" name="options[price][]" value="" placeholder="0" /></div>
                                <div class="option-min"><input type="text" name="options[min][]" value="" placeholder="0" /></div>
                                <div class="option-max"><input type="text" name="options[max][]" value="" placeholder="0" /></div>
                                <div class="option-delete"><a class="button" style="display: none;"><?php echo __( 'Delete', 'yith-woocommerce-product-add-ons' );?></a></div>
                                <div class="option-description"><input type="text" name="options[description][]" value="" placeholder="Description" /></div>
                                <div class="option-default"><input type="checkbox" name="options[default][]" value="<?php echo $i ;?>" class="new_default" /> <?php echo __( 'Checked', 'yith-woocommerce-product-add-ons' );?></div>
                                <div class="option-required"><input type="checkbox" name="options[required][]" value="<?php echo $i ;?>" class="new_required" /> <?php echo __( 'Required', 'yith-woocommerce-product-add-ons' );?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7"><i><?php echo __( 'Choose an option to see more options ;)', 'yith-woocommerce-product-add-ons' );?></i></td>
                        </tr>
                    </table>
                </div>
            </div>

        <?php endif; ?>

        <div class="form-row">
            <div class="options">
                <i><?php echo __( 'Save the "Add-on" if you want to add new options.', 'yith-woocommerce-product-add-ons' );?></i>
            </div>
        </div>
        <div class="form-row">

            <?php if( $is_edit ) : ?>

                <div class="delete" style="color: #a00; float: right;">
                    <input type="checkbox" name="del" value="1"> <?php echo __( 'Delete this Add-on', 'yith-woocommerce-product-add-ons' );?>
                    <span class="dashicons dashicons-trash"></span>
                </div>

            <?php endif; ?>

            <div class="submit">
                <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php $is_edit ? _e( 'Save add-on', 'yith-woocommerce-product-add-ons' ) : _e( 'Save new add-on', 'yith-woocommerce-product-add-ons' );?>">
                <?php if( ! $is_edit ) : ?>
                    <a href="#" class="button cancel"><?php echo __( 'Cancel', 'yith-woocommerce-product-add-ons' );?></a>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <div class="clear"></div>

</form>
